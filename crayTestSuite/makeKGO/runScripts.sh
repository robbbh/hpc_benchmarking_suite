#!/bin/bash

# This script allows for the bulk gathering of benchmark data. 
# it is a member of the HPC Benchmarking Suite.

# This brings the system profile into scope (knows where qsub etc are)
. /etc/profile
echo -e "Executing runScripts.sh"

# WARNING: THE USER MUST PROVIDE THE ROOT PATH TO SUITE DIRECTORY TO MAKE KGO'S
# IF THIS ISN'T DEFINED, MAKING KGO'S WILL NOT WORK WITH CRONTAB    
# EXAMPLE: 
#       connect the following path "/small/home/d05/rharris" or "~"   
#       with "crayTestSuite/makeKGO/mvaPichMakeKGO" 
#       the root path in this case is "~/trunk" which concatonates to
#       "/small/home/d05/rharris/trunk/crayTestSuite/makeKGO/mvaPichMakeKGO""

# define the root path here
export rootPath=~/trunk

# gets the users id and checks if their file system exists
user=$(whoami)
eval ". ~$user/.bashrc"
ls ~ > /dev/null
RES=$?

### WallClock Times ###
pt2ptTime2=00:05:00
collectiveTime4=00:10:00
collectiveTime8=00:15:00
collectiveTime16=00:35:00
collectiveTime32=00:45:00
collectiveTime64=01:10:00
collectiveTime128=01:40:00
epccCollectiveTime4=01:00:00
epccCollectiveTime8=01:00:00
epccCollectiveTime16=01:00:00
epccPt2ptTime2=00:30:00

mvapichCollective=1
mvapichPt2pt=1
epccCollective=1
epccPt2pt=1

# if the error return code from the file system check is not 0, the scripts wont run. 
if [ $RES == 0 ]; then
    export mainDirecName=$(date -u +%Y-%m-%dT%H-%M-%SZ)

    if [ $mvapichPt2pt -eq 1 ]; then        

        ## WARNING: pwd is will be different when the script is executed directly from 
        ## its directory compared to when it's run through crontab. 

        # pwd from crontab is "/small/home/d05/rharris"
        # pwd from inside the direc is "/small/home/d05/rharris/trunk/crayTestSuite/makeKGO"

        export MVAPichSuiteDir="crayTestSuite/makeKGO/mvaPichMakeKGO"
        cd $rootPath/$MVAPichSuiteDir

        # Point to point must always run on 2 nodes
        export pt2ptNodes=2
        export pt2ptJOBID=$(qsub -V -l select="$pt2ptNodes":coretype=broadwell -l walltime=$pt2ptTime2 -j oe -o ./pt2pt/$mainDirecName ./getPt2ptData.sh 2>&1)
        echo -e "Pt2pt job, $pt2ptJOBID with $pt2ptNodes nodes."

    fi


    if [ $mvapichCollective -eq 1 ]; then
  
        export MVAPichSuiteDir="crayTestSuite/makeKGO/mvaPichMakeKGO"
        cd $rootPath/$MVAPichSuiteDir

        # Adjust the nodes in the "nodes" list to edit the number of nodes 
        # you would like to gather data for. 
        # Note: must be n = n * 2 with n starting at 4.

        export colNodes='4 8 16 32 64 128'
        for input in $colNodes;
                do

                    export collectiveNodes=$input
                    # concatonates the walltime variable based off the node count
                    eval "timeVar=\$collectiveTime$input"
                    export collectiveJOBID=$(qsub -V -l select="$collectiveNodes":coretype=broadwell -l walltime=$timeVar -j oe -o ./collective/$collectiveNodes/$mainDirecName ./getCollectiveData.sh 2>&1)
                    echo -e "Collecitve job, $collectiveJOBID with $collectiveNodes nodes."

                done
        fi

# ----------------------------------------------------------------------------

    if [ $epccCollective -eq 1 ]; then     

        export epccSuiteDir="crayTestSuite/makeKGO/epccMakeKGO"

        cd $rootPath/$epccSuiteDir

        export epccNodes='4 8 16'
        for input in $epccNodes;
                do

                    export epccCollectiveNodes=$input
                    # concatonates the walltime variable based off the node count
                    eval "timeVar=\$epccCollectiveTime$input"
                    export epccCollectiveJOBID=$(qsub -V -l select="$epccCollectiveNodes":coretype=broadwell -l walltime=$timeVar -j oe -o ./collective/$epccCollectiveNodes/$mainDirecName ./getEpccCollectiveData.sh 2>&1)
                    echo -e "EPCC Collecitve job, $epccCollectiveJOBID with $epccCollectiveNodes nodes."

                done        

    fi

    if [ $epccPt2pt -eq 1 ]; then

        export epccSuiteDir="crayTestSuite/makeKGO/epccMakeKGO"

        cd $rootPath/$epccSuiteDir

        export epccPt2ptNodes=2
        export epccPt2ptJOBID=$(qsub -V -l select="$epccPt2ptNodes":coretype=broadwell -l walltime=$epccPt2ptTime2 -j oe -o ./pt2pt/$epccPt2ptNodes/$mainDirecName ./getEpccPt2ptData.sh 2>&1)
        echo -e "EPCC Pt2pt job, $epccPt2ptJOBID with $epccPt2ptNodes nodes."

        fi



else
        # Cannot find the users directory
        echo -e "Cannot find $user's home directory"
fi
