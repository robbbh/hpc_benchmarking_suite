#!/bin/bash
#PBS -N mvapichCollect
#PBS -q parallel


set -e

export MPICH_RANK_REORDER_DISPLAY=1
export execPath="../../benchmarks/osu-micro-benchmarks-5.1/mpi/collective"
export metaName=$(echo $(date) | sed -e 's/\s/_/g' )
export bulkDataPath="../mvaPichMakeKGO/collective/$collectiveNodes"
export uniqueJobPath="../mvaPichMakeKGO/collective/$collectiveNodes/$mainDirecName"
declare -i noMPIt 
declare -i TnoMPIt
# Number of MPI tasks per node
noMPIt=32 
# Total number of MPI tasks
# Gets number of nodes from the parent shell
TnoMPIt=$(echo "$(($collectiveNodes * $noMPIt))")

cd $PBS_O_WORKDIR

mkdir $uniqueJobPath
export executable="osu_allreduce osu_bcast osu_gather osu_scatter osu_alltoallv" 

echo '' >> $uniqueJobPath/$metaName.temp 
cat /critical/opfc/hall_status_oper >> $uniqueJobPath/$metaName.temp 
echo '' >> $uniqueJobPath/$metaName.temp 

	for task in $executable 
	do

        ## Appends the job name to the job dump file
        echo '' >> $uniqueJobPath/$metaName.temp 
        echo $task >> $uniqueJobPath/$metaName.temp 
        echo '' >> $uniqueJobPath/$metaName.temp 

            ## Runs the executable and outputs it to a file
            aprun -n $TnoMPIt -N $noMPIt $execPath/$task > $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/^$/d' $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/#/d' $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/Application/d' $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat
 
            cat $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat >> $bulkDataPath/${task}_run_${noMPIt}_${TnoMPIt}.txt
            echo '' >> $bulkDataPath/${task}_run_${noMPIt}_${TnoMPIt}.txt   

        echo $PBS_JOBID >> $uniqueJobPath/$metaName.temp 
        cat $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat >> $uniqueJobPath/$metaName.temp 
        echo '' >> $uniqueJobPath/$metaName.temp         
        
        done 

echo '' >> $uniqueJobPath/$metaName.temp 
qstat_snapshot >> $uniqueJobPath/$metaName.temp 
echo '' >> $uniqueJobPath/$metaName.temp 

