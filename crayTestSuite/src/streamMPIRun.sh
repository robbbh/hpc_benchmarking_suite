#!/bin/bash
#PBS -N streamMPI
#PBS -q parallel


export OMP_NUM_THREADS=1
export mainJobDir="$workingDir/streamMPIData"
export subJobDir="$mainJobDir/$streamMPINodes/"
export filePath="streamMPIData/$streamMPINodes/$OMP_NUM_THREADS"

mkdir "$mainJobDir"
mkdir "$subJobDir"
mkdir "$subJobDir/$OMP_NUM_THREADS"

set -e

export MPICH_RANK_REORDER_DISPLAY=1
export execPath="$currentDir/../benchmarks/stream/streamMPI/streamMPI"
export bench='Copy Scale Add Triad'
export NumOfMPITasks=36
export arraySize=4294967296
export totalNumOfMPITasks=$(echo "$(($streamMPINodes * $NumOfMPITasks))")

cd $PBS_O_WORKDIR

export taskSplit=$(echo "$NumOfMPITasks/2" | bc)

if [ $taskSplit -eq 0 ]; then
    export taskSplit=1
fi

aprun -n $totalNumOfMPITasks -N $NumOfMPITasks -cc cpu -S $taskSplit $execPath \
1> $PBS_O_WORKDIR/$filePath/streamMPI_total_${totalNumOfMPITasks}_${NumOfMPITasks}.txt

for type in $bench;
    do

        grep "$type" $PBS_O_WORKDIR/$filePath/streamMPI_total_${totalNumOfMPITasks}_${NumOfMPITasks}.txt \
        | awk '{print "'"$arraySize"'" " " $2}' > $PBS_O_WORKDIR/$filePath/streamMPI_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
        # Add metadata to the live run file
        echo "# Threads = $OMP_NUM_THREADS" >> $PBS_O_WORKDIR/$filePath/streamMPI_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
        echo "# MPI Tasks = $NumOfMPITasks" >> $PBS_O_WORKDIR/$filePath/streamMPI_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
        echo "# Total MPI Tasks = $totalNumOfMPITasks" >> $PBS_O_WORKDIR/$filePath/streamMPI_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
        echo "# Nodes = $streamMPINodes" >> $PBS_O_WORKDIR/$filePath/streamMPI_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat


    done 

rm $PBS_O_WORKDIR/$filePath/streamMPI_total_${totalNumOfMPITasks}_${NumOfMPITasks}.txt

# Writes the local completion file
touch "$PBS_O_WORKDIR/streamMPIData/$streamMPINodes/.success"
# Writes to the global completion file
echo "streamMPI" >> $PBS_O_WORKDIR'/.successCheck'
