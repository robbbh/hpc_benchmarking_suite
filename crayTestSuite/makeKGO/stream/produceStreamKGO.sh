#!/bin/bash

export streamMPINodes=32
export streamMPI1JOBID=$(qsub -V -l select="$streamMPINodes":coretype=broadwell -l walltime=00:05:00 -j oe -o ./mpi/ ./getStreamMPI.sh)
echo "STREAM MPI get KGO job submitted with jobID $streamMPI1JOBID"

export streamOMPNodes=32
export streamOMP1JOBID=$(qsub -V -l select="$streamOMPNodes":coretype=broadwell -l walltime=00:05:00 -j oe -o ./omp/ ./getStreamOMP.sh)
echo "STREAM OMP get KGO job submitted with jobID $streamOMP1JOBID"
