Developer Guide
===============

**Who is this guide for?** 

This guide is for a *developer* of the HPC benchmarking suite. It's aim is to 
guide the developer on how to correctly configure benchmark's running parameters
to their specific needs. 

There will be a future developer guide on how to develop your own KGO, 
it's unfortunately not available at this time.

.. toctree::
    :maxdepth: 2

    developerConfigureSubmit.rst

