#!/bin/bash
#PBS -N EPCC_Pt2pt
#PBS -q parallel

set -e

# workingDir $ curentDir are exported from the parent shell
export OMP_NUM_THREADS=1
export mainJobDir="$workingDir/epccPt2ptData"
export subJobDir="$mainJobDir/$epccPt2ptNodes"


# If the main directory hasn't been created, create it
if [ ! -d "$mainJobDir" ]; then
    mkdir "$mainJobDir"
fi 
mkdir "$subJobDir"

export execPath="../../../benchmarks/epcc_mixedMode_Benchmarks/mixedModeBenchmark"
export execInputPath="../../../src/epccConfig/configure_pt2pt.txt"


declare -i task

export OMP_STACKSIZE=2G
export OMP_WAIT_POLICY=PASSIVE
export MPICH_MAX_THREAD_SAFETY=multiple
ulimit -s unlimited
# broadwell nodes have 36 cores max
export maxCores=36
export nodeCount=$epccPt2ptNodes
export MPITaskSet=2
export ThreadTaskSet=$epccOMPTasks


cd $PBS_O_WORKDIR


ulimit -s unlimited

loopNo=0
for thread in $ThreadTaskSet; 
     do

        let totalMPItasks=$(echo "$MPITaskSet*$nodeCount" | bc)
        let loopNo=$loopNo+1
        let splitNuma=$MPITaskSet/2

        export OMP_NUM_THREADS=$thread
        export filePath="epccPt2ptData/$epccPt2ptNodes/$OMP_NUM_THREADS"

        # MPI = 36 18 12 6 4 2
        # OMP = 1 2 3 6 9 18

        mkdir "$subJobDir/$OMP_NUM_THREADS"

        aprun -n $totalMPItasks -N $MPITaskSet -cc numa_node -ss -d $OMP_NUM_THREADS -S $splitNuma \
        $execPath $execInputPath \
        > $PBS_O_WORKDIR/$filePath/epcc_Pt2pt_${loopNo}.txt



         sed -i -e '/Application/d' $PBS_O_WORKDIR/$filePath/epcc_Pt2pt_${loopNo}.txt

        # Must pass the job output along with the number of nodes and MPI tasks
         ../../epccConfig/mixedModeParser.py $PBS_O_WORKDIR/$filePath/epcc_Pt2pt_${loopNo}.txt \
         $epccPt2ptNodes $MPITaskSet

    done

    # Writes to the local completion file 
    touch "$PBS_O_WORKDIR/epccPt2ptData/$epccPt2ptNodes/.success"
    # Writes to the global completion file
    echo "epccPt2pt" >> $PBS_O_WORKDIR'/.successCheck'

