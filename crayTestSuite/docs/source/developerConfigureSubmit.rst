Configuring the Suite Submission Scripts
========================================

Configuring MVAPICH Collective Node Counts
------------------------------------------

Below is the default node configuration for the MVAPICH collective benchmark which can be found in **/src/submitBench.sh**. 

.. code-block:: bash

    export nodes='4 8' # 16 32 64 128'
    for input in $nodes;
        do

            export collectiveNodes=$input
            eval "timeVar=\$collectiveTime$input"
            eval "export collective${input}JOBID=\$(qsub -V -l select=\"\$collectiveNodes\":coretype=broadwell -l walltime=\"\$timeVar\" -j oe -o \"\$workingDir\" $currentDir/collectiveRun.sh 2>&1)"

        done    

By default it's configured to complete a 4 and 8 node run, however 
there is currently support for 16, 32, 64 and 128. These can be enabled
by simply editing the **nodes** variable before the loop.

To enable all supported node counts, simply remove the comment
hashtag and encapsulate all numbers within the quotation marks.

>>> export nodes='4 8 16 32 64 128'

Adjusting the Qstat Frequency
-----------------------------

The submission script allows for the real time tracking of the jobs  
through qstat while they're running on the HPC. It can inform the user of
the jobs' running status **(Queuing, Running, Exiting)** and whether the job has finished or failed. 
Although a useful tool, qstat can be very resource heavy on the qstat server. 
To mitigate this, there is an option to adjust the frequency of the qstat request. 
By default it is set to 60 seconds, however the user can increase or decrease it based 
on their estimate on the jobs run time. 

To change the qstat request time, simply change the "queueCycleTime" variable. 
The queue cycle time is represented in seconds and therefore requires the input in seconds.
(2 minutes = 120 seconds)    

.. code-block:: bash
    
    # Time in seconds
    queueCycleTime=60

.. Note:: 
    The Job tracker is automatically run when a job is submitted, however the jobs are 
    not dependent on this tracker. If the user wished to end the tracking process, the 
    benchmark submissions will not be effected. The user will have to run the comparison 
    process manually if the tracking is ended. Please see the :doc:`user guide </user_guide_basic/>`
    for a guide on manually running the comparison.
