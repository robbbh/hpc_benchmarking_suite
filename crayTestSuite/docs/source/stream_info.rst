STREAM Benchmark
================

About
-----
The STREAM benchmark is a simple synthetic benchmark program that measures sustainable memory bandwidth (in MB/s) and the corresponding computation rate for simple vector kernels. 
The benchmark completes a COPY, SCALE, SUM and TRIAD calculation and measures the bandwidth in (MB/s). These 4 calculations are represented as 1 benchmark in the suite, with both an 
OpenMP and MPI version available for the user to test. 

.. parsed-literal::

    ------------------------------------------------------------------
    name        kernel                  bytes/iter      FLOPS/iter
    ------------------------------------------------------------------
    COPY:       a(i) = b(i)                 16              0
    SCALE:      a(i) = q*b(i)               16              1
    SUM:        a(i) = b(i) + c(i)          24              1
    TRIAD:      a(i) = b(i) + q*c(i)        24              2
    ------------------------------------------------------------------

Website
-------
https://www.cs.virginia.edu/stream/
