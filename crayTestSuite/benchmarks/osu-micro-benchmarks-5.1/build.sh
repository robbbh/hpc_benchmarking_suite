#!/bin/bash
# This file should allow the osu-micro-benchmarks-5.1 to succesfully compile on the Cray XC40.
./configure CC=cc CXX=CC
perl -pi -e 's/^(CFLAGS =.*)/\1 -h upc/g' ./upc/Makefile
make
