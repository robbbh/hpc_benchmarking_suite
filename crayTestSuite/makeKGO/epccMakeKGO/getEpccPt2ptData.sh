#!/bin/bash
#PBS -N EPCC_Pt2pt
#PBS -q parallel

set -e

export execPath="../../benchmarks/epcc_mixedMode_Benchmarks/mixedModeBenchmark"
export metaName=$(echo $(date) | sed -e 's/\s/_/g' )
export bulkDataPath="./pt2pt/$epccPt2ptNodes"
export uniqueJobPath="./pt2pt/$epccPt2ptNodes/$mainDirecName"


#set variables as Intergers
declare -i  MPItasks

export maxThreads=18
export OMP_NUM_THREADS=1
export OMP_STACKSIZE=2G
export OMP_WAIT_POLICY=PASSIVE
export MPICH_MAX_THREAD_SAFETY=multiple
ulimit -s unlimited
export MPItasks=2
export nodeCount=$epccPt2ptNodes
export totalMPItasks=$(echo "$(($nodeCount * $MPItasks))")

cd $PBS_O_WORKDIR

mkdir $uniqueJobPath

ulimit -s unlimited

loopNo=0
while [ $OMP_NUM_THREADS -le "$maxThreads" ]
     do

        aprun -n $totalMPItasks -N $MPItasks -cc numa_node -d $OMP_NUM_THREADS\
         $execPath configure_pt2pt.txt\
         > $uniqueJobPath/epcc_Pt2pt_${loopNo}.txt

        echo -e "Total num of MPI tasks: $totalMPItasks" 
        echo -e "MPI Tasks: $MPItasks"
        echo -e "Threads: $OMP_NUM_THREADS"
        echo -e "Iteration Num: $loopNo\n"

        sed -i -e '/Application/d' $uniqueJobPath/epcc_Pt2pt_${loopNo}.txt

        # PATH: /home/d05/rharris/trunk/crayTestSuite/makeKGO/epccMakeKGO/collective/4/name
        ./mixedModeParser.py $uniqueJobPath/epcc_Pt2pt_${loopNo}.txt

        let OMP_NUM_THREADS=$OMP_NUM_THREADS*2 
        let loopNo=$loopNo+1


        # Switch between powers of 3 to 2

        if [ "$OMP_NUM_THREADS" -eq "16" ]; then
            let OMP_NUM_THREADS=18
        fi

    done

