EPCC Mixed Mode Benchmarks
==========================

About
-----

With the current prevalence of multi-core processors in HPC architectures, mixed-mode programming, using both MPI and OpenMP in the same application is becoming increasingly important. We have designed and implemented a set of low-level microbenchmarks to test the performance of this programming model.

The mixed-mode microbenchmarks provide analogues for the typical operations found in MPI microbenchmark suites, for both point-to-point and collective communication patterns.
Two main considerations are captured in the microbenchmarks:

*  The effects of intra-thread communication and synchronisation by measuring both the times of MPI library calls and the reading and writing of buffers.
*  The ability to easily compare the effects of changing the OpenMP thread to MPI process ratio while using the same total number of cores. This is done by the appropriate choice of buffer sizes and message lengths. 

Benchmarks Used
---------------

======================= ======================================================================================================================================================================================================================
 Benchmark Name          Description
======================= ======================================================================================================================================================================================================================
MasteronlyPingpong 	      Pingpong between two MPI processes with reads and writes by all OpenMP threads under each process. MPI communication takes place outside OpenMP parallel regions.
FunnelledPingpong 	      Same as MasteronlyPingpong but MPI communication takes inside OpenMP parallel region by master thread.
MultiplePingpong 	      Same as MasteronlyPingpong except all threads take part in MPI communication.
MasteronlyHaloexchange 	  All MPI processes are arranged in a ring and each process exchanges messages with its two neighbouring processes with parallel reads and writes the OpenMP threads under each process. Communication takes place outside of the parallel region.
FunnelledHaloexchange 	  Same as above but communication is carried out inside the the OpenMP parallel regions by the master thread.
MultipleHaloexchange 	  Same as MasteronlyHaloexchange except inter-process communication is carried out inside the the OpenMP by all threads.
MasteronlyMultipingpong   Every core on a node performs a pingpong with the corresponding core on another node. MPI messages are sent/received outside of the parallel regions.
FunnelledMultipingpong 	  Same as MasteronlyMultipingpong but MPI messages are carried out inside the parallel region by the master thread.
MultipleMultipingpong 	  Same as MasteronlyMultipingpong with MPI messages carried out inside the parallel region by all threads.
Barrier 	              Mixed-mode barrier where threads under each process first synchronise with an OMP BARRIER followed by an MPI_Barrier to synchronise each MPI process.
Reduce 	                  Mixed mode reduce benchmark. All threads under every MPI process combines its local buffer. All MPI processes then combined their values to get the overall reduction value which is stored on the master MPI process.
AllReduce 	              Mixed mode allreduce benchmark. All threads under every MPI process combines its local buffer. All MPI processes then perform and MPI_Allreduce on these values to give the overall reduction value at each process.
Broadcast 	              Data is broadcast from the master MPI process to all other MPI processes and then copied by each OpenMP thread.
Gather 	                  Mixed mode gather benchmark. All threads under an MPI process writes to a specific portion of a buffer. The master MPI process then gathers all the data using an MPI_Gather.
AlltoAll 	              Mixed mode all to all benchmark. Each OpenMP thread sends/receives a portion of data to/from every other thread.
======================= ======================================================================================================================================================================================================================

License
-------

+--------------------------------------------------------------------------------------+
|                                                                                      |
|                                                                                      |
|        **Mixed-mode OpenMP/MPI MicroBenchmark Suite - Version 1.0**                  |
|                                                                                      |
|        produced by Mark Bull, Jim Enright and Fiona Reid at                          |
|                                                                                      | 
|        Edinburgh Parallel Computing Centre                                           |
|                                                                                      |
|        email: markb@epcc.ed.ac.uk, fiona@epcc.ed.ac.uk                               |
|                                                                                      |
|                                                                                      |
|        Copyright 2012, The University of Edinburgh                                   |
|                                                                                      |
|                                                                                      |
|        Licensed under the Apache License, Version 2.0 (the "License");               |
|        you may not use this file except in compliance with the License.              |
|        You may obtain a copy of the License at                                       |
|                                                                                      |
|        http://www.apache.org/licenses/LICENSE-2.0                                    |
|                                                                                      |
|        Unless required by applicable law or agreed to in writing, software           |
|        distributed under the License is distributed on an "AS IS" BASIS,             |
|        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      |
|        See the License for the specific language governing permissions and           |
|        limitations under the License.                                                |
|                                                                                      |
|                                                                                      |
+--------------------------------------------------------------------------------------+

Website
-------

http://www2.epcc.ed.ac.uk/~markb/mpiopenmpbench/intro.html
