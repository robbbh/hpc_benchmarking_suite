#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import numpy as np
import sys
import os
import glob

def main():
    """ This scripts takes CSV file arguments of the STREAM
        benchmark data and produces sensible ranges to be inputed into
        the HPC benchmarking suite KGO's. """

    filePaths = sys.argv[1:]

    if len(filePaths) < 1:
        print "Please supply a csv file path argument."
        sys.exit()
    else:
        for benchmarkFilePath in filePaths:
            openCSVFile(benchmarkFilePath)
    
    
def openCSVFile(csvFilePath):
    """ Open the CSV file containing the STREAM data """

    streamResults = []
    try:
        with open(csvFilePath, "r") as dataInputFromFile:
            for benchmarkData in dataInputFromFile:
                temp = benchmarkData.strip("\n").strip(",")
                streamResults.append(float(temp))

    except:
        print "{0} does not exist. Please supply a valid file path.".format(
                                                                csvFilePath)

    produceStandardDeviation(streamResults, csvFilePath)


def produceStandardDeviation(dataArray, csvFilePath):
    """ Coverts the array into a numpy array and produces the 
        standard deviation """

    numpyDataArray = np.array(dataArray)
    average = np.average(numpyDataArray)
    standardDeviation = np.std(numpyDataArray, dtype=np.float64)

    defineKGORanges(standardDeviation, average, csvFilePath)
    

def defineKGORanges(standardDeviation, average, csvFilePath):
    """ Define a sensible variance of 5 sigma to the left and 
        right of the normal (average). """

    sigma = 5
    varianceRange = standardDeviation * sigma
    upperBound = average + varianceRange
    lowerBound = average - varianceRange

    KGO_Range = "{0} {1}".format(lowerBound, upperBound)
    writeToFile(KGO_Range, csvFilePath)


def writeToFile(KGO_Input, csvFilePath):
    """ Writes the KGO's produced to file for the user to input
        into the KGO directory files. """ 

    filePath = buildOutputPath(csvFilePath)

    with open(filePath, "a") as line:
        line.write("KGO for the the following file %s\n" % (csvFilePath))
        line.write("%s\n\n" % (KGO_Input))


def buildOutputPath(csvFilePath):
    """ Places the KGO output into the correct directory """

    outputFilePath = "STREAM_KGO.dat"

    if "omp" in csvFilePath:
        outputFilePath = os.path.join("omp","STREAM_omp_KGO.dat")
    if "mpi" in csvFilePath:
        outputFilePath = os.path.join("mpi","STREAM_mpi_KGO.dat")

    return outputFilePath


if __name__ == "__main__":
    main()


