User Guide
==========

**Who is this guide for?** 

This guide is for a *user* of the HPC benchmarking suite. It will aim 
to cover basic use of the suite, and how the user can use/ configure 
it for their own use. 

Alternatively, if you're looking for a guide to *develop* the suite, or 
find this guide not sufficient to your needs, then you may want to look at
the :doc:`developer guide </developer_guide>`.


**A run through of the suite by example**

This guide will cover how to use the suite in its basic form, 
while also giving some background on how the suite works and makes its decisions.  

.. toctree::
    :maxdepth: 2

    user_guide_basic
