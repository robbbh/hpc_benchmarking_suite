#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

# This file is a part of the HPC Benchmarking suite

""" This scipt provides the core comparison functionality to the
HPC benchmarking suite and uses a set of pre difined KGO ranges for
Broadwell chips within a Cray XC40. The KGO is configurable, however
requires the user to perform bulk tests themselves and to set
their own ranges in wake of any hardware/ major configuration
changes. Further documentation will outline how to gather
and apply the new ranges (developer guide).
"""

import numpy
import sys
import os
import glob
import datetime
import re
# suppresses scientific notation e+04 etc
numpy.set_printoptions(suppress=True, precision=2)

# Global Variables Here.
ERRORDICT = {}
SUMMARYARR = []
STDOUTDICT = {}

ERRORBOOL = False
OUTPUTBOOL = False
SUMMARYBOOL = False

METADATA = ()
BENCHOUTPUT = ""

FAILCOUNT = 0
PASSFAILCOUNT = 0
PASSCOUNT = 0

SCRIPTBASEPATHUNIQUE = ""
SCRIPTBASEPATH = ""


# gets the full path of the current working directory
# prints /small/home/d05/rharris/trunk/crayTestSuite/src/
SCRIPTBASEPATH = os.path.dirname(os.path.realpath(__file__))

def main():
    """ This function will take all command line agruments
        and build directory paths with them. This function will
        then call checkArglen which will check the paths validility. """

    systemArguments = sys.argv[:]
    suficientArgumentCheck(systemArguments[:])
    specifiedDirIn = systemArguments[1]
    benchArgs = systemArguments[2:]
    extraArgsLen = len(systemArguments[1:])

    # builds a full path containing the directory name
    global SCRIPTBASEPATHUNIQUE
    SCRIPTBASEPATHUNIQUE = (
        os.path.join(SCRIPTBASEPATH, "jobsRun", specifiedDirIn)
        )
    # prints... /small/home/d05/rharris/trunk/crayTestSuite/ &
    # src/jobsRun/2015-11-26T16-40-18Z

    # Checks the amount of arguments given
    checkArgumentLength(extraArgsLen, benchArgs)


    # Writes results to file and screen.
    printOverview()
    writeSummaryReportDecision()
    writeOutputReportDecision()
    writeErrorReportDecision()


def suficientArgumentCheck(inputNum):
    """  Checks if an argument is given. If no argument is given, 
        then the program will exit   """
    if len(inputNum) <= 1:
        errorMessage = (
            "Error, invalid number of arguments provided. User provided <= %d"
            " arguments, where > 1 are expected.\nProgram "
            "Terminated." % len(inputNum)
            )
        print errorMessage
        sys.exit()


def checkArgumentLength(inLen, specifiedBenchmarks):
    """ Check if the directory specified exists. Will then
        check the user arguments lengths and decide whether to 
        crawl the run directory for valid benchmarks types
        or build only the user specified ones. """

    global SCRIPTBASEPATHUNIQUE
    benchmarkNames = [x[0] for x in os.walk(SCRIPTBASEPATHUNIQUE)]

    # Checks whether there are any benchmarks in the jobsRun directory
    if benchmarkNames:
        if inLen is 1:
            getBenchmarkTypes(benchmarkNames)
        elif inLen > 1:
            buildDirectoryPaths(specifiedBenchmarks)

    else:
        print "No valid benchmarks found in the directory specified. \
               \nProgram Terminated."
        sys.exit()


def getBenchmarkTypes(benchmarkNames):
    """  If directories contain the "Data" keyword then it will add them to a 
         list so directory paths can be built.  """

    dataDirectoryFound = False
    trimmedName = []
    for add in benchmarkNames:
        # Finds all directories with the "Data" string pattern at the end
        benchmark = re.findall(r"\w+\/((?!\w+Data/\d+)\w+Data)", add)
        if benchmark:
            dataDirectoryFound = True
            benchList = "".join(benchmark[0])
            trimmedName.append(re.sub('Data', '', benchList))

    if not dataDirectoryFound:
        print "No benchmark Data directories found.\nProgram Terminating."
        sys.exit()

    buildDirectoryPaths(trimmedName)


def buildDirectoryPaths(benchmarkList):
    """  Builds the paths from the inputs of benchmark names and
    prepares them to be checked. """

    global SCRIPTBASEPATHUNIQUE, SCRIPTBASEPATH, BENCHOUTPUT
    KGDPath = []
    KGDPathData = []
    livePath = []
    livePathData = []
    nodeCount = []
    threadCount = []
    benchType = ""
    lines = ""

    for benchmark in benchmarkList:
        if os.path.exists((os.path.join(SCRIPTBASEPATH, "kgo", benchmark))):
            # Gets the correct node amounts to append to directory paths. 
            nodeCount = getNodeCount(
                    os.path.join(SCRIPTBASEPATHUNIQUE, benchmark+"Data"))

            for nodes in nodeCount:
                benchType = benchmark + " Benchmarks"
                BENCHOUTPUT = "\n\n|{1:-<25}{0:^30s}{1:-<25}|\n".format(
                                                        benchType, lines)
                print BENCHOUTPUT

                # Gets the correct thread amounts to append to directory paths. 
                threadCount = getThreadCount(
                    os.path.join(SCRIPTBASEPATHUNIQUE, benchmark + "Data", 
                                                                       nodes))

                for threads in threadCount:
                    livePath.append(os.path.join(SCRIPTBASEPATHUNIQUE, 
                                                          benchmark + "Data")
                                   )

                    livePathData = \
                        glob.glob(os.path.join(
                                  SCRIPTBASEPATHUNIQUE, benchmark + "Data",
                                                     nodes, threads, "*.dat")
                                  )

                    KGDPath.append(os.path.join(SCRIPTBASEPATH, "kgo", 
                                                   benchmark, nodes, threads)
                                  )

                    KGDPathData = (
                        glob.glob(os.path.join
                                  (SCRIPTBASEPATH, "kgo", benchmark, nodes,
                                                            threads, "*.dat")
                                  )
                                  )

                    checkPathValidity(KGDPath, KGDPathData, livePath,
                                                                 livePathData)
        else:
            errorMessage = ("\nERROR = The live data directory %s doesn't "
                            "exist. This may be due to the KGO for the "
                            "particular node count doesn't exist or the "
                            "argument specified doesn't exist. Please ensure "
                            "the directory specified exists. Benchmark has "
                            "been skipped.\n"
                            % (os.path.join(SCRIPTBASEPATH, benchmark))
                            )
            print errorMessage


def checkPathValidity(KGDPath, KGDPathData, livePath, liveDataPath):
    """ This function will use the directory paths built in the 
        buildDirectoryPaths function and check their validity. 
        If the directory exists, it will then check for any files present. 
        If files for live data and KGO data exist then the paths will 
        be opened."""

    liveDirPath = livePath[0]
    KGDdirPath = KGDPath[0]

    KGDPathDataIn = sorted(KGDPathData)
    liveDataPathIn = sorted(liveDataPath)

    for KGDdataPath, liveDataPath in zip(KGDPathDataIn, liveDataPathIn):
        # check if KGO directories and data exist 
        if os.path.exists(KGDdirPath) is True and KGDdataPath:
            # check if live directories and data exist
            if os.path.exists(liveDirPath) is True and liveDataPath:
                openFiles(liveDataPath, KGDdataPath)

            elif os.path.exists(liveDirPath) is True and not liveDataPath:
                errorMessage = (
                    "\nERROR = The live data files %s don't exist. This "
                    "Please ensure the files specified exist. Benchmark has "
                    "been skipped.\n" % liveDataPath)
                print errorMessage

            else:
                errorMessage = (
                    "\nERROR = The live data directory %s doesn't exist. This "
                    "may be due to the KGO doesn't exist or the argument "
                    "specified doesn't exist. Please ensure the directory"
                    "specified exists. Benchmark has"
                    "been skipped.\n" % liveDirPath)
                print errorMessage

        elif os.path.exists(KGDdirPath) is True and not KGDdataPath:
            errorMessage = (
                "\nERROR = The known good data files %s don't exist.  Please "
                "ensure the files specified exist. Benchmark has been "
                "skipped.\n" % KGDdataPath)
            print errorMessage


def openFiles(liveData, KGOData):
    """ File paths are opened here. The input files will
        be split into 2 arrays for each file opened. The live data file
        will be split by size and data content, whereas the KGO will be split
        by lower and upper bound arrays. The live file in will have its 
        metadata stripped and placed into individual variables. """

    liveDataPaths = str(liveData)
    KGODataPaths = str(KGOData)
    liveDataArray = []
    lowerBound = []
    upperBound = []
    sizeOfData = []
    threads = None
    totalMpiTasks = None
    mpiTasks = None
    nodes = None 
    metaData = ()

    with open(liveDataPaths, "r") as dataFile:
        for liveRunData in dataFile:
            # Extracts metadata from the live data
            if (liveRunData.startswith('#')):  
                metaDataTemp = liveRunData
                metaDataTemp = metaDataTemp.strip('#').replace(" ","")\
                               .replace("\n","").split("=")

                if (metaDataTemp[0] in "Threads"):
                    threads = metaDataTemp[1]
                elif (metaDataTemp[0] in "MPITasks"):
                    mpiTasks = metaDataTemp[1]
                elif (metaDataTemp[0] in "TotalMPITasks"):
                    totalMpiTasks = metaDataTemp[1]
                elif (metaDataTemp[0] in "Nodes"):
                    nodes = metaDataTemp[1]

            # splits the size of data and their values into seperate arrays
            else:
                splitSize = liveRunData.split()
                sizeOfData.append(int(splitSize[0]))
                liveDataArray.append(float(splitSize[1]))

        # place metadata into a tuple
        metaData = (nodes, totalMpiTasks, mpiTasks, threads)
        setMetaData(metaData)

    with open(KGODataPaths, "r") as KGODataFile:
        # splits the KGO data into upper and lower bounds
        for KGO in KGODataFile:
            splitRange = KGO.split()
            lowerBound.append(float(splitRange[0]))
            upperBound.append(float(splitRange[1]))

    # Once the data has been opened and sorted, it is sent off to be compared
    compareData(
        liveDataArray, sizeOfData, lowerBound, upperBound, liveDataPaths)


def compareData(liveArrayIn, sizeOfDataIn, lowerBoundIn, upperBoundIn, liveIn):
    """ This function will compare each dataset against one another. 
        It will then sort the errornous and regular data into output arrays
        ready to be written to file."""

    outOfRangeResults = []
    errors = []
    output = []
    errorList = ""

    # converts the lowerBound/ upperBound/ liveData arrays into a numpy array
    lowerBound = numpy.array(lowerBoundIn)
    upperBound = numpy.array(upperBoundIn)
    liveDataIn = numpy.array(liveArrayIn)

    for size, live, upperValue, lowerValue in zip(
            sizeOfDataIn, liveDataIn, upperBound, lowerBound):

        if (live > upperValue or live < lowerValue):
            outOfRangeResults.append(live)
            errorList = size, lowerValue, live, upperValue
            errors.append(errorList)

        # Appends the output into a tuple
        outList = size, lowerValue, live, upperValue
        output.append(outList)

    numberOfErrors = len(outOfRangeResults)
    decisionResult(numberOfErrors, errors, output, liveIn)


def decisionResult(numErrors, errorsOutput, allDataOutput, liveIn):
    """
    Will make the decision to whether the benchmark passed, failed or 
    passed with errors.

    .. Note:: The current configuration is dependant on the benchmark and
        is set automatically. If it can't set a value, it'll be set to 
        1. 
    """

    global FAILCOUNT, PASSFAILCOUNT, PASSCOUNT
    benchName = os.path.basename(liveIn)
    errorOutput = []
    allOutput = []
    # need to explicitly format the tuple otherwise it'll print "2.00" as "2."
    tupleFormat = "{0:11} {1:^20.7f} {2:^20.7f} {3:^20.7f}"
    concern = 1
    fail = 1

    # Gets the correct concern and fail threshold for the specific benchmark
    concern, fail = getDecisionThreshDict(benchName)

    # appends every evaluated data value into an output array
    for dataLine in allDataOutput:
        allOutput.append(tupleFormat.format(*dataLine))
    setTotalOutput(allOutput, liveIn)

    # Decides the benchmark's run state (pass,fail,pass \w fail)
    if numErrors >= concern and numErrors < fail:
        message = "\n%s\n\tPassed with %d Error(s)" % (benchName, numErrors)
        result = "Passed with %d Error(s)" % numErrors
        PASSFAILCOUNT += 1

        # Appends to the error log
        for error in errorsOutput:
            errorOutput.append(tupleFormat.format(*error))
        setErrorLog(errorOutput, liveIn)

    elif numErrors >= fail:
        message = "\n%s\n\tFailed with %d Error(s)" % (benchName, numErrors)
        result = "Failed with %d Error(s)" % numErrors
        FAILCOUNT += 1

        # Appends to the error log
        for error in errorsOutput:
            errorOutput.append(tupleFormat.format(*error))
        setErrorLog(errorOutput, liveIn)

    else:
        message = "\n%s\n\tPassed" % benchName
        result = "Passed"
        PASSCOUNT += 1

    printDecisionOutput(benchName, result)


def printDecisionOutput(benchNameIn, resultIn):
    """ Will take the inputs required for screen output and format
        them appropriately. Output has been placed into a single
        variable to easily pass to the summary write."""

    underscore = " {0:-<80}"
    middleFormat = " {0:-<15}+{0:-<22}+{0:-<20}+{0:-<20} "
    metadataFormat = "|{0:^15s}|{1:^22s}|{2:^20s}|{3:^20s}|"
    nameFormat = "|{0:^25s}|{1:^54s}|"

    lines = underscore.format("")
    middle = middleFormat.format("")
    name = nameFormat.format("Benchmark Name", benchNameIn)
    result = nameFormat.format("Result", resultIn)
    metaDataNames = metadataFormat.format(
        "Nodes","Total MPI Tasks","MPI Tasks","Threads")
    metaData = metadataFormat.format(
        getMetaData()[0],getMetaData()[1],getMetaData()[2],getMetaData()[3])

    output = "\n{0}\n{2}\n{0}\n{3}\n{0}\n{4}\n{1}\n{5}\n{0}".format(
            lines, middle, name, result, metaDataNames, metaData)

    print output

    setSummary(output, benchNameIn)


def printOverview():
    """ Will print the Summary containing the total number of benchmarks,
        fail rate, pass rate and pass \w fail rate"""

    # define formatters
    underscore = " {0:-<55}"
    nameFormat = "|{0:^24s}|{1:^30s}|"
    summaryFormat = "|{0:^55s}|"
    # define variables
    lines = underscore.format("")
    title = summaryFormat.format("Quick Summary")
    passCount = nameFormat.format("Pass", str(PASSCOUNT))
    passFailCount = nameFormat.format("Pass \w Errors", str(PASSFAILCOUNT))
    failCount = nameFormat.format("Fail", str(FAILCOUNT))
    totalCount = nameFormat.format("Total Benchmarks Run", 
                                str(FAILCOUNT + PASSFAILCOUNT + PASSCOUNT))

    print "\n\n{0}\n{1}\n{0}\n{2}\n{0}\n{3}\n{0}\n{4}\n{0}\n{5}\n{0}".format(
            lines, title, passCount, passFailCount, failCount, totalCount)


def getDecisionThreshDict(benchName):
    """ Will get the pass/ fail threshold for each individual benchmark.
    The user must add a Pass/ Fail threshold for each new benchmark type
    added. """

    # the tuple contains the concern rate followed by the fail rate. 
    decValues = {'osu': (1,3),
                 'stream': (1,1),
                 'epcc': (1,2),
                 };

    for benchmarkType in decValues.keys():
        if benchmarkType in benchName:
            concern = decValues.get(benchmarkType)[0]
            error = decValues.get(benchmarkType)[1]

    return concern, error


def getNodeCount(pathIn):
    """
    This ensures the correct KGO data is always chosen. It does this 
    by crawling through the live data directory and getting the 
    directory names which correspond to node and thread amounts. 
    It will return these amounts in an array such as: [4,8,16,32,64] 
    nodes / threads. """

    nodeDirectories = [x[0] for x in os.walk(pathIn)]
    nodes = []
    threads = []

    # '/small/home/d05/rharris/trunk/crayTestSuite/src/jobsRun/
    # 2016-04-11T13-08-24Z/pt2ptData/4/1'
    # 4 is the node count and 1 is the thread count

    for nodeNumber in nodeDirectories[1:]:
        try:
            if type(int(nodeNumber.split("/")[-2])) is not int:
                continue
        except:
            nodes.append(str(nodeNumber.split("/")[-1]))
    return nodes


def getThreadCount(pathIn):
    """
        This ensures the correct KGO data is always chosen. It does this 
        by crawling through the live data directory and getting the 
        directory names which correspond to node and thread amounts. 
        It will return these amounts in an array such as: [4,8,16,32,64] 
        nodes / threads. """

    threadDirectories = [x[0] for x in os.walk(pathIn)]
    threads = []

    for threadNumber in threadDirectories[1:]:
        try:
            if type(int(threadNumber.split("/")[-1])) is int:
                threads.append(str(threadNumber.split("/")[-1]))
        except:
            continue
    return threads


def setMetaData(metaDataIn):
    """ Contains meta data related to the job. It'll read this 
        metadata in from the live job """
    global METADATA
    METADATA = metaDataIn


def getMetaData():
    """ Gets the metadata that is in the form of a tuple. """
    return METADATA


def setErrorLog(errorIn, jobName):
    """  Sets the error Log. This will be called from inside the
         "decisionResult()" function """
    global ERRORBOOL
    ERRORBOOL = True
    ERRORDICT[jobName] = errorIn


def getErrorLog():
    """  Gets the error Log. This will be called from writeErrorReport()
        during the file writing process  """
    return ERRORDICT


def setSummary(outIn, jobName):
    """  Sets the compare summary. This will be called from inside the
         "decisionResult()" function. Sets a key based off the job name and 
         sets its value as outName """
    global SUMMARYBOOL
    SUMMARYBOOL = True
    temp = [BENCHOUTPUT, outIn]
    SUMMARYARR.append(temp)


def getSummary():
    """  Gets the summary. This will be called from writeSummary()
        during the file writing process  """
    return SUMMARYARR


def setTotalOutput(stdOutIn, jobName):
    """  Sets the stadard output. This will be called from inside the
         "decisionResult()" function """
    global OUTPUTBOOL
    OUTPUTBOOL = True
    STDOUTDICT[jobName] = stdOutIn


def getTotalOutput():
    """  Gets the standard output. This will be called from writeOutReport()
        during the file writing process  """
    return STDOUTDICT


def writeErrorReportDecision():
    """  Evaluates the decision flag and writes and error report if True   """
    if ERRORBOOL is True:
        writeErrorReport()
        print "\n{0:-<10} {1:} {0:-<10}".format(
            "","A data error report has been produced in the job directory")


def writeOutputReportDecision():
    """  Evaluates the decision flag and writes and error report if True   """
    if OUTPUTBOOL is True:
        writeOutReport()
        print "\n{0:-<10} {1:} {0:-<10}".format(
            "","A data output report has been produced in the job directory")


def writeSummaryReportDecision():
    """  Evaluates the decision flag and writes and error report if True   """
    if SUMMARYBOOL is True:
        writeSummary()
        print "\n{0:-<10} {1:} {0:-<10}".format(
            "","A summary report has been produced in the job directory")


def writeErrorReport():
    """ Writes a report containing errorneous values and sizes """
    # SCRIPTBASEPATHUNIQUE is the job directory file.
    global SCRIPTBASEPATHUNIQUE
    lineLength = "-"*82
    columnFormat = "\n|{0:^15s}|{1:^19s}|{2:^19s}|{3:^19s}|\n"
    dateTime = datetime.datetime.now()
    fileName = os.path.join(SCRIPTBASEPATHUNIQUE, "errorLog.txt")
    outputDictionary = getErrorLog()
    with open(fileName, "w") as line:
        line.write("Error output from the %s job. " % (SCRIPTBASEPATHUNIQUE))
        line.write("\n%s\n\n" % dateTime)
        for jobName, errors in outputDictionary.items():
            line.write("%s\n" % jobName)
            
            line.write(columnFormat.format("Size","Lower","Value","Upper"))
            for err in errors:
                line.write("\n|%s\t|" % err)
            line.write("\n\n%s\n\n" % lineLength)


def writeOutReport():
    """ Writes an output report containing all output """
    # SCRIPTBASEPATHUNIQUE is the job directory file.
    global SCRIPTBASEPATHUNIQUE
    fileName = os.path.join(SCRIPTBASEPATHUNIQUE, "outputLog.txt")
    columnFormat = "\n|{0:^12s}|{1:^13s}|{2:^14s}|{3:^13s}|\n"
    lineLength = "-"*50
    dateTime = datetime.datetime.now()
    outputDictionary = getTotalOutput()
    with open(fileName, "w") as line:
        line.write("Standard output from the %s job" % (SCRIPTBASEPATHUNIQUE))
        line.write("\n%s\n\n" % dateTime)
        for jobName, output in outputDictionary.items():
            line.write("%s\n" % jobName)
            line.write(columnFormat.format("Size","Lower","Value","Upper"))
            for out in output:
                line.write("\n|%s\t|" % out)
            line.write("\n\n%s\n\n" % lineLength)


def writeSummary():
    """ Writes a summary report to file which """
    # SCRIPTBASEPATHUNIQUE is the job directory file. 
    global SCRIPTBASEPATHUNIQUE
    filename = os.path.join(SCRIPTBASEPATHUNIQUE, "summary.txt")
    lineLength = "-"*50
    dateTime = datetime.datetime.now()
    outputDictionary = getSummary()
    with open(filename, "w") as line:
        line.write("Summary from the %s job. " % (SCRIPTBASEPATHUNIQUE))
        line.write("\n%s" % dateTime)
        for i in SUMMARYARR:
            line.write(i[:][0])
            line.write(i[:][1])

if __name__ == "__main__":

    main()

