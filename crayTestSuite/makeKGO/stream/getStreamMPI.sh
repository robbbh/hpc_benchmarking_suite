#!/bin/bash
#PBS -N stream_MPI
#PBS -q parallel

export OMP_NUM_THREADS=1

cd $PBS_O_WORKDIR

export MPITasks='36'
# Array size of 4GB
export ArraySize='4294967296' 
direcName="mpi"
type='Copy Scale Add Triad'
numTests=$streamMPINodes

executablePath='../../benchmarks/stream/streamMPI/streamMPI'

# Deletes any old content in the mpi directory
rm ./$direcName/*

for size in $ArraySize;
    do

        for tasks in $MPITasks;
            do

            export taskSplit=$(echo "$tasks/2" | bc)

            if [ $taskSplit -eq 0 ];
            then
                export taskSplit=1
            fi


            num=1
            numTestsMinus=$(($numTests-1))
            # runs the same job on "numTests" amount of nodes. 
            while [ $num -le $numTestsMinus ]; 
                do
                    aprun -n $tasks -N $tasks -cc cpu -S $taskSplit $executablePath > "./$direcName/MPI_Tasks_${num}_${tasks}.txt" &
                    let num=$num+1
                done
                aprun -n $tasks -N $tasks -cc cpu -S $taskSplit $executablePath > "./$direcName/MPI_Tasks_${num}_${tasks}.txt"

            # cat each file together into one large file
            num2=1
            while [ $num2 -le $numTests ];
                do
                    cat ./$direcName/MPI_Tasks_${num2}_${tasks}.txt >> "./$direcName/Appended_${tasks}_MPI_Tasks.txt"
                    let num2=$num2+1
                done

            # Gets the value for each test type and gets the average based on "numTests" amount of runs
            for bench in $type;
                do
                    grep "$bench" ./$direcName/Appended_${tasks}_MPI_Tasks.txt | awk '{print $2 "," }' >> ./$direcName/${bench}_dump.csv
                done 


            done
            rm ./$direcName/MPI_Tasks_*.txt
    done

python produceStreamRanges.py ./mpi/*.csv
