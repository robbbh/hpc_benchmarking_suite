MVAPICH Benchmarks
==================

Benchmarks
----------

Below are descriptions of the MVAPICH benchmarks used in this HPC Benchmarking Suite. 
Although the MVAPICH package contains tens of benchmarks, the suite uses only 10.
5 are point to point benchmarks which only ever run on 2 nodes, where the other 5 are
collective benchmarks which can be run on 4 - 128 nodes.  

Point-to-Point MPI Benchmarks
-----------------------------

This suite contains support for the following pt2pt benchmarks;

* osu_latency - Latency Test

     The latency tests are carried out in a ping-pong fashion. The sender
     sends a message with a certain data size to the receiver and waits for a
     reply from the receiver. The receiver receives the message from the sender
     and sends back a reply with the same data size. Many iterations of this
     ping-pong test are carried out and average one-way latency numbers are
     obtained. Blocking version of MPI functions (MPI_Send and MPI_Recv) are
     used in the tests. This test is available here.

* osu_latency_mt - Multi-threaded Latency Test

     The multi-threaded latency test performs a ping-pong test with a single
     sender process and multiple threads on the receiving process. In this test
     the sending process sends a message of a given data size to the receiver
     and waits for a reply from the receiver process. The receiving process has
     a variable number of receiving threads (set by default to 2), where each
     thread calls MPI_Recv and upon receiving a message sends back a response
     of equal size. Many iterations are performed and the average one-way
     latency numbers are reported. This test is available here.

* osu_bw - Bandwidth Test

     The bandwidth tests were carried out by having the sender sending out a
     fixed number (equal to the window size) of back-to-back messages to the
     receiver and then waiting for a reply from the receiver. The receiver
     sends the reply only after receiving all these messages. This process is
     repeated for several iterations and the bandwidth is calculated based on
     the elapsed time (from the time sender sends the first message until the
     time it receives the reply back from the receiver) and the number of bytes
     sent by the sender. The objective of this bandwidth test is to determine
     the maximum sustained date rate that can be achieved at the network level.
     Thus, non-blocking version of MPI functions (MPI_Isend and MPI_Irecv) were
     used in the test. This test is available here.

* osu_bibw - Bidirectional Bandwidth Test

     The bidirectional bandwidth test is similar to the bandwidth test, except
     that both the nodes involved send out a fixed number of back-to-back
     messages and wait for the reply. This test measures the maximum
     sustainable aggregate bandwidth by two nodes. This test is available here.


* osu_multi_lat - Multi-pair Latency Test (requires threading support from MPI-2)

     This test is very similar to the latency test. However, at the same 
     instant multiple pairs are performing the same test simultaneously.
     In order to perform the test across just two nodes the hostnames must
     be specified in block fashion.

Collective MPI Benchmarks
-------------------------

This suite contains support for the following collective benchmarks;

* osu_allreduce     - MPI_Allreduce Latency Test
* osu_alltoallv     - MPI_Alltoallv Latency Test
* osu_bcast         - MPI_Bcast Latency Test
* osu_gather        - MPI_Gather Latency Test
* osu_scatter       - MPI_Scatter Latency Test

Collective Latency Information;

 The latest OMB version includes benchmarks for various MPI blocking 
 collective operations (MPI_Allgather, MPI_Alltoall, MPI_Allreduce, 
 MPI_Barrier, MPI_Bcast, MPI_Gather, MPI_Reduce, MPI_Reduce_Scatter, 
 MPI_Scatter and vector collectives). These benchmarks work in the
 following manner.  Suppose users run the osu_bcast benchmark with N
 processes, the benchmark measures the min, max and the average latency of
 the MPI_Bcast collective operation across N processes, for various
 message lengths, over a large number of iterations. In the default
 version, these benchmarks report the average latency for each message
 length. Additionally, the benchmarks offer the following options:

* "-f" can be used to report additional statistics of the benchmark,
       such as min and max latencies and the number of iterations.

* "-m" option can be used to set the maximum message length to be used in a
       benchmark. In the default version, the benchmarks report the
       latencies for up to 1MB message lengths.

* "-x" can be used to set the number of warmup iterations to skip for each
       message length.

* "-i" can be used to set the number of iterations to run for each message
       length.

* "-M" can be used to set per process maximum memory consumption.  By
       default the benchmarks are limited to 512MB allocations.


Author Information
------------------

| Ohio-State University
| Contact: Dhabaleswar K. (DK) Panda
| Dept of Computer Science and Engineering
| 2001-2016 NBCL. All rights reserved.
| 774 Dreese Laboratories
| 2015 Neil Avenue
| Columbus, OH 43210

Website
-------
http://mvapich.cse.ohio-state.edu/
