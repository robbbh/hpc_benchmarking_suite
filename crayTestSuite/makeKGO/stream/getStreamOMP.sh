#!/bin/bash
#PBS -N stream_OMP
#PBS -q parallel

ulimit -s unlimited
export OMP_STACKSIZE=2G
export OMP_WAIT_POLICY=PASSIVE
export CRAY_OMP_CHECK_AFFINITY=TRUE
export OMP_NUM_THREADS=1

cd $PBS_O_WORKDIR

export Threads='36'
# Array size of 4GB
export ArraySize='4294967296' 
direcName="omp"
executablePath='../../benchmarks/stream/streamOMP/streamOMP'
ls $executablePath
type='Copy Scale Add Triad'
numTests=$streamOMPNodes

# Deletes any old content in the omp directory
rm ./$direcName/*

for size in $ArraySize;
    do
        for threads in $Threads;
            do

            num=1
            numTestsMinus=$(($numTests-1))
            # runs the same job on "$numTests"amount of nodes. 
            while [ $num -le $numTestsMinus ]; 
                do
                    export OMP_NUM_THREADS=$threads  
                    aprun -n 1 -N 1 -cc cpu -d $threads $executablePath > ./$direcName/OMP_THREADS_${num}_${threads}.txt &
                    let num=$num+1
                done
                    export OMP_NUM_THREADS=$threads  
                    aprun -n 1 -N 1 -cc cpu -d $threads $executablePath > ./$direcName/OMP_THREADS_${num}_${threads}.txt 

            # cat each file together into one large file
            num2=1
            while [ $num2 -le $numTests ];
                do
                    cat ./$direcName/OMP_THREADS_${num2}_${threads}.txt >> "./$direcName/Appended_${threads}_OMP_Threads.txt"
                    let num2=$num2+1
                done

            # Gets the value for each test type and gets the average based on "numTests" amount of runs
            for bench in $type;
                do
                    grep "$bench" ./$direcName/Appended_${threads}_OMP_Threads.txt | awk '{print $2 "," }' >> ./$direcName/${bench}_dump.csv
                done 


            done
            rm ./$direcName/OMP_THREADS_*.txt
    done

python produceStreamRanges.py ./omp/*.csv
