Interpreting Your Graphed Data
==============================

Locating the KGO files
----------------------

Once the user has produced their graph, they can then set their own KGO ranges. The KGO ranges 
can be found inside the **./src/kgo** directory, and are will be in the directory representing 
the benchmark run. 

The directories will contain sub directories of KGO ranges for each node and thread configuration,
as seen by the structure below;

.. parsed-literal::
    kgo_directory -> benchmark_name -> node_count -> thread_count -> kgo_file

.. code-block:: text

    |-- collective
    |   |-- 128
    |   |   `-- 1
    |   |       |-- osu_allreduce_run_32_4096_KGO.dat
    |   |       |-- osu_alltoallv_run_32_4096_KGO.dat
    |   |       |-- osu_bcast_run_32_4096_KGO.dat
    |   |       |-- osu_gather_run_32_4096_KGO.dat
    |   |       `-- osu_scatter_run_32_4096_KGO.dat
    |   |-- 16
    |   |   `-- 1
    |   |       |-- osu_allreduce_run_32_512_KGO.dat
    |   |       |-- osu_alltoallv_run_32_512_KGO.dat
    |   |       |-- osu_bcast_run_32_512_KGO.dat
    |   |       |-- osu_gather_run_32_512_KGO.dat
    |   |       `-- osu_scatter_run_32_512_KGO.dat
    |   |-- 32
    |   |   `-- 1
    |   |       |-- osu_allreduce_run_32_1024_KGO.dat
    |   |       |-- osu_alltoallv_run_32_1024_KGO.dat
    |   |       |-- osu_bcast_run_32_1024_KGO.dat
    |   |       |-- osu_gather_run_32_1024_KGO.dat
    |   |       `-- osu_scatter_run_32_1024_KGO.dat
    |   |-- 4
    |   |   `-- 1
    |   |       |-- osu_allreduce_run_32_128_KGO.dat
    |   |       |-- osu_alltoallv_run_32_128_KGO.dat
    |   |       |-- osu_bcast_run_32_128_KGO.dat
    |   |       |-- osu_gather_run_32_128_KGO.dat
    |   |       `-- osu_scatter_run_32_128_KGO.dat
    |   |-- 64
    |   |   `-- 1
    |   |       |-- osu_allreduce_run_32_2048_KGO.dat
    |   |       |-- osu_alltoallv_run_32_2048_KGO.dat
    |   |       |-- osu_bcast_run_32_2048_KGO.dat
    |   |       |-- osu_gather_run_32_2048_KGO.dat
    |   |       `-- osu_scatter_run_32_2048_KGO.dat
    |   `-- 8
    |       `-- 1
    |           |-- osu_allreduce_run_32_256_KGO.dat
    |           |-- osu_alltoallv_run_32_256_KGO.dat
    |           |-- osu_bcast_run_32_256_KGO.dat
    |           |-- osu_gather_run_32_256_KGO.dat
    |           `-- osu_scatter_run_32_256_KGO.dat


Taking the graph that was produced earlier, we can interpret and extract good KGO ranges from it. The graph represents 
a 64 node alltoallv benchmark, and therefore we would edit the **./osu_alltoallv_run_32_2048_KGO.dat** file in the 
**/64/1/** directory.  

.. image:: osu_alltoallv_run_32_2048.png

.. _setting-kgo:

Setting the KGO
---------------

We can find the kgo files within **/src/kgo** with each file following the lower / upper column format. 
Whereas some KGO files can contain multiple sizes, other can contain only a single data size i.e: STREAM.
As STREAM benchmark KGO's upper and lower bounds are automatically produced after STREAM KGO submission, 
the user will have to copy the range values given into the correct KGO file.

The range file for the graph above would would look similar to the following;

.. code-block:: text

    0.0 25000.00
    0.0 25000.00
    0.0 25000.00
    0.0 25000.00
    0.0 25000.00
    0.0 25000.00
    0.0 25000.00
    0.0 25000.00
    0.0 30000.00
    0.0 42000.00
    0.0 50000.00
    0.0 80000.00
    0.0 100000.00
    0.0 520000.00
    0.0 540000.00
    0.0 850000.00
    0.0 1800000.00
    0.0 3600000.00
    0.0 7500000.00

The first column represents the lower bound whereas the second column represents the upper bound. This KGO file has been 
set up to only flag lower than expected performance and will not fail if performance is better than expected (lower bound
has therefore been set to 0.0). This can be changed however by setting the first column to a sensible value. By setting 
both bounds, the suite could identify unexpected increases in performance which could then be investigated by the user. 

.. Note:: Depending on whether the benchmark measures network bandwidth or latency; benchmarks measuring latency will
    have no value in the lower bound, whereas benchmarks measuring bandwidth will have a static upper bound (second column).  

Interpreting the graph, a sensible lower bound for the alltoallv benchmark could be;

.. code-block:: text

    7500.0 25000.00
    7500.0 25000.00
    7500.0 25000.00
    7500.0 25000.00
    7500.0 25000.00
    7500.0 25000.00
    7500.0 25000.00
    7500.0 25000.00
    7500.0 30000.00
    20000.0 42000.00
    20000.0 50000.00
    30000.0 80000.00
    70000.0 100000.00
    420000.0 520000.00
    420000.0 540000.00
    550000.0 850000.00
    1200000.0 1800000.00
    2600000.0 3600000.00
    5500000.0 7500000.00


