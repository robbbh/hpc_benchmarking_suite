Benchmark Submission Scripts
============================

    .. Warning::
        It is strongly advised that the user **NOT** significantly  modify the below scripts, as it may break or cause 
        unexpected errors when executing the suite. 

MVAPICH Pt2pt Submission Script
-------------------------------

Below is collective submission script which is executed by **submit.sh**. 

.. literalinclude:: ../../src/pt2ptRun.sh
   :language: bash


MVAPICH Collective Submission Script
------------------------------------

Below is collective submission script which is executed by **submit.sh**. 

.. literalinclude:: ../../src/collectiveRun.sh
   :language: bash

EPCC Collective Mixed Mode Submission Script
--------------------------------------------

Below is collective submission script which is executed by **submit.sh**. 

.. literalinclude:: ../../src/epccCollectiveRun.sh
   :language: bash

EPCC Pt2pt Mixed Mode Submission Script
---------------------------------------

Below is collective submission script which is executed by **submit.sh**. 

.. literalinclude:: ../../src/epccPt2ptRun.sh
   :language: bash

STREAM OMP Submission Script
----------------------------

Below is collective submission script which is executed by **submit.sh**. 

.. literalinclude:: ../../src/streamOMPRun.sh
   :language: bash

STREAM MPI Submission Script
----------------------------

Below is collective submission script which is executed by **submit.sh**. 

.. literalinclude:: ../../src/streamMPIRun.sh
   :language: bash
