#!/bin/bash
#PBS -N mvapichP2P
#PBS -q parallel

# workingDir is exported from the parent shell
export OMP_NUM_THREADS=1
export mainJobDir="$workingDir/pt2ptData"
export subJobDir="$mainJobDir/$pt2ptNodes/"
export filePath="pt2ptData/$pt2ptNodes/$OMP_NUM_THREADS"

mkdir "$mainJobDir"
mkdir "$subJobDir"
mkdir "$subJobDir/$OMP_NUM_THREADS"

set -e

export MPICH_MAX_THREAD_SAFETY="multiple"
export OMP_NUM_THREADS=1
export MPICH_RANK_REORDER_DISPLAY=1
export execPath="$currentDir/../benchmarks/osu-micro-benchmarks-5.1/mpi/pt2pt"


declare -i noMPIt 
declare -i TnoMPIt
# Number of MPI tasks per node
noMPIt=1 
# Total number of MPI tasks
# Gets number of nodes from the parent shell
TnoMPIt=$(echo "$(($pt2ptNodes * $noMPIt))")

cd $PBS_O_WORKDIR

export executable="osu_bibw osu_bw osu_latency osu_multi_lat osu_latency_mt" 

   for task in $executable
	do
            aprun -n $TnoMPIt -N $noMPIt $execPath/$task 1> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/Application/d' $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/#/d' $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            # Add metadata to the live run file
            echo "# Threads = $OMP_NUM_THREADS" >> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            echo "# MPI Tasks = $noMPIt" >> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            echo "# Total MPI Tasks = $TnoMPIt" >> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            echo "# Nodes = $pt2ptNodes" >> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat

        done
    # Writes the local completion file
    touch "$PBS_O_WORKDIR/pt2ptData/$pt2ptNodes/.success"
    # Writes to the global completion file
    echo "pt2pt" >> $PBS_O_WORKDIR'/.successCheck'

