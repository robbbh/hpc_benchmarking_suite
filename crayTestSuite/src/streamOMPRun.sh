#!/bin/bash
#PBS -N streamOMP
#PBS -q parallel

export OMP_NUM_THREADS=36
export mainJobDir="$workingDir/streamOMPData"
export subJobDir="$mainJobDir/$streamOMPNodes/"
export filePath="streamOMPData/$streamOMPNodes/$OMP_NUM_THREADS"

mkdir "$mainJobDir"
mkdir "$subJobDir"
mkdir "$subJobDir/$OMP_NUM_THREADS"

set -e

ulimit -s unlimited
export OMP_STACKSIZE=2G
export OMP_WAIT_POLICY=PASSIVE
export CRAY_OMP_CHECK_AFFINITY=TRUE
export execPath="$currentDir/../benchmarks/stream/streamOMP/streamOMP"
export MPICH_RANK_REORDER_DISPLAY=1
export bench='Copy Scale Add Triad'
export NumOfMPITasks=1
export totalNumOfMPITasks=$(echo "$(($streamOMPNodes * $NumOfMPITasks))")
# Array size of 4GB
export ArraySize=4294967296

cd $PBS_O_WORKDIR

aprun -n $totalNumOfMPITasks -N $NumOfMPITasks -cc cpu -d $OMP_NUM_THREADS  $execPath \
> $PBS_O_WORKDIR/$filePath/streamOMP_total_${totalNumOfMPITasks}_${NumOfMPITasks}.txt

for type in $bench;
do

    grep "$type" $PBS_O_WORKDIR/$filePath/streamOMP_total_${totalNumOfMPITasks}_${NumOfMPITasks}.txt \
    | awk '{print "'"$ArraySize"'" " " $2}' > $PBS_O_WORKDIR/$filePath/streamOMP_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
    # Add metadata to the live run file
    echo "# Threads = $OMP_NUM_THREADS" >> $PBS_O_WORKDIR/$filePath/streamOMP_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
    echo "# MPI Tasks = $NumOfMPITasks" >> $PBS_O_WORKDIR/$filePath/streamOMP_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
    echo "# Total MPI Tasks = $totalNumOfMPITasks" >> $PBS_O_WORKDIR/$filePath/streamOMP_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
    echo "# Nodes = $streamOMPNodes" >> $PBS_O_WORKDIR/$filePath/streamOMP_${type}_${totalNumOfMPITasks}_${NumOfMPITasks}.dat
    

done 

rm $PBS_O_WORKDIR/$filePath/streamOMP_total_${totalNumOfMPITasks}_${NumOfMPITasks}.txt

# Writes the local completion file
touch "$PBS_O_WORKDIR/streamOMPData/$streamOMPNodes/.success"
# Writes to the global completion file
echo "streamOMP" >> $PBS_O_WORKDIR'/.successCheck'


