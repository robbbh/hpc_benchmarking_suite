.. HPC_Benchmark_Suite documentation master file, created by
   sphinx-quickstart on Thu Dec  3 12:38:15 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HPC Benchmarking Suite's documentation!
==================================================

**About the HPC Benchmarking Suite**

The HPC benchmarking suite is a collection of open source benchmarks packaged together into one convenient suite. Using previously gathered KGO the user can run the suite, and compare live performance against expected performance. This can indicate whether the HPC is not performing as expected, allow for mitigating solutions to be implemented. The suite will output a simple display with the conclusion indicating a Pass or Fail result for each benchmark instance. 

.. image:: ./xc40.png

**Content**

Below you can find user guides and the source code for both the Python and Bash scripts used in this suite. The python script was used for the comparison functionality, whereas the BASH script was used to create the job submission script.

Guides:

.. toctree::
   :maxdepth: 2

   user_guide
   developer_guide
   KGO_guide

Script Documentation and Source Code:

.. toctree::
    :maxdepth: 1

    compare
    makeHistogram
    produceStreamRanges
    bash_source
    
Benchmark Information:

.. toctree::
    :maxdepth: 1
    
    mvapich_info.rst
    epcc_info.rst
    stream_info.rst
    



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

