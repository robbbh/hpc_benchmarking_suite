#!/usr/bin/env python2.7
# -*- coding: iso-8859-1 -*-

import os
import sys
import re
import numpy as np
import matplotlib as plt2
import matplotlib.pyplot as plt


def main():
    ''' Takes the input arguments and produces a histogram from
        the bulk data  '''

    for arg in sys.argv[1:]:
        print "Sorting {}".format(arg)
        sortedData, dataSizes = sortData(arg)
        makeGraph(sortedData, dataSizes, arg[:-4])


def sortData(fileName):
    ''' Opens the bulk data ".txt" file and sorts the data into
        2D arrays based on data size. '''

    source = fileName
    try:
        storeData = []
        temp = []
        sizeFlag = False
        dataSize = []
        # Could use a .split() to seperate the size and data
        # This would nullify the need to recalculate the size
        # after stripping it with the regex.

        with open(source, "r") as inp:
            for line in inp:
                # if it encounters a new line it will strip and storeData
                # the array in the "storeData" array.
                if line in ['\n', '\r\n']:
                    sizeFlag = True
                    # print "Data Sizes: %s" % dataSize
                    storeData.append([float(x.strip()) for x in temp])
                    # delete the contents of the array after appending its
                    # data to the storeData array after each iteration
                    del temp[:]
                    continue
                # returns a "match" object as out which contains the
                # "data" column in the bulk
                sizeDataColumn = line.split()[0]
                outDataColumn = line.split()[1]
                # Will continue if "out" is a blank line
                if outDataColumn and sizeDataColumn is None:
                    print "Hit"
                    continue
                # appends the regex pattern to the array
                temp.append(outDataColumn)
                # Ensures the data size is only set once.
                if sizeFlag is False:
                    dataSize.append(sizeDataColumn)

                
            # Sorts data by size into into a size specific array
            # converts the zipped tuple into a 2D array
            # print storeData
            storage = np.array(zip(*storeData))
            return storage, dataSize

    except IOError:
        print "Could not find data file: ", source
        sys.exit(1)
    except (EOFError, KeyboardInterrupt):
        print "User aborted the execution"


def makeGraph(data, plots, name):
    ''' Makes a subplot of histograms using matplotlib. This method
        contains all of the formatting for the subpots, including colour,
        resolution and spacing '''

    # gets the title and data type for each subplot
    subPlotName, subPlotUnitName = makeAxisNames(name, plots)
    plt.close('all')
    # number of bins the histograms have
    n_bins = 50
    # Sets the number of subplots
    plotsNum = plots
    saveType = '.png'
    plotName = name + saveType
    colours = ['green', 'greenyellow']

    # fig size is set to 25.5 x 16 inches (1920 x 1200)
    fig = plt.figure(figsize=(25.6, 16))
    # subplot spacing
    fig.subplots_adjust(left=0.06, bottom=0.07, right=0.98,
                        top=0.93, wspace=0.23, hspace=0.55)
    fig.suptitle(name)

    # each data size is plotted into its histogram subplot.
    for i in range(len(plots)):
        sp1 = fig.add_subplot(5, 5, i+1)
        sp1.hist(data[i], n_bins, normed=0, histtype='bar',
                 color=colours[i % 2])
        sp1.set_title(subPlotName[i])
        sp1.grid()
        sp1.set_ylabel("Number of Items in Bin")
        sp1.set_xlabel(subPlotUnitName)
        plt.xticks(rotation=90)
    #plt.show()
    # Saves the plot in the location of the input files.
    plt.savefig(plotName, format='png')
    print '{} has been saved to file'.format(plotName)


def makeAxisNames(nameIn, plots):
    ''' Contains The correct Unit Values for each benchmarks. Will be decided
        based on the benchmarks name.'''

    # Dictionary which contains benchmark start size and data type.
    benchmarkMetadata = {
        "osu_scatter": ("Microsecond (us)"),
        "osu_gather": ("Microsecond (us)"),
        "osu_bcast": ("Microsecond (us)"),
        "osu_alltoallv": ("Microsecond (us)"),
        "osu_allreduce": ("Microsecond (us)"),
        "osu_multi_lat": ("Microsecond (us)"),
        "osu_latency": ("Microsecond (us)"),
        "osu_bibw": ("(MB/s)"),
        "osu_latency_mt": ("Microsecond (us)"),
        "osu_bw": ("(MB/s)"),
        "epcc_AllReduce": ("Seconds (s)"),
        "epcc_Alltoall": ("Seconds (s)"),
        "epcc_Broadcast": ("Seconds (s)"),
        "epcc_Gather": ("Seconds (s)"),
        "epcc_FunnelledHaloexchange": ("Seconds (s)"),
        "epcc_Reduce": ("Seconds (s)"),
        "epcc_MasteronlyHaloexchange": ("Seconds (s)"),
        "epcc_MultipleHaloexchange": ("Seconds (s)"),
        "epcc_MultiplePingpongIntra": ("Seconds (s)"),
        "epcc_MasteronlyPingpongIntra": ("Seconds (s)"),
        "epcc_FunnelledPingpongIntra": ("Seconds (s)")
        }

    # Could probably optimise this to get the key without iterating
    # the whole thing
    for key in benchmarkMetadata:
        if key in nameIn:
            unitName = benchmarkMetadata[key]
            break
        else:
            unitName = "Unit Name"

    subPlotTitles = produceTitle(plots)
    return subPlotTitles, unitName


def produceTitle(sizes):
    ''' Produces the correct sizes and builds a name for each subplot
         for each plot.   '''

    sizeRange = []
    fullName = []
    for i in sizes:
        name = "Message Size is {0} Bytes".format(i)
        sizeRange.append(name)

    return sizeRange


if __name__ == "__main__":

    main()
