#!/bin/bash
set -e

# temp variable is needed to ensure integer expression is used
# when evaluating the variables in logical statements/ comparisons

declare -i temp 
declare -i tempInqueue
declare -i tempFlag
declare -i tempFinished
declare -i tempNumOfJobs
declare -i jobsComplete

# Please note, this script relies on PBS qstat to get its status info. 
# This while loop controls the PBS queue feedback and works by checking 
# for 2 "/.success" files that are produced at the end of each run script.
# The "if" statements are able to give queue status info by "greping" 
# the status columns value.

# It can differentiate between a succesfully, unsuccesfuully and a job 
# in progress


# This function will take arguments from successCheck containing benchmark 
# names. It will then do 1 qstat query with the job IDs 1 after each other
# as arguments. This will result in only 1 qstat being needed regardless 
# of the number of JOBs submitted. 
function generalQStat {


    # This builds the benchmark ID variable name to be used in qstat,
    # by appending "JOBID" to the end
    tempQstatArg="$(echo ${1} | perl -ple 's/\w+/\$$&JOBID/g')"
    # When a benchmark is queried after it has finished, it will output 
    # to standard error and return error code 35. (This will be caught by 
    # set -e thus killing the program. Therefore we re-assign the standard 
    # error to true to counter this.
    eval "qstatQuery=\"\$(qstat $tempQstatArg 2>&1 || true)\""

    # iterates over the benchmark name which is used as a key to build
    # other variable names.
    for benchName in $(echo "${1}") ;
    do
      # gets the number of nodes to build the correct path to check for success file.
      export nodeDirectory="$(echo "$benchName" | perl -ne 'm/(\d+)(?!.*\d)/g; print "$1"')"
      # Get the benchmarks basename by striping the node count
      export striptBenchName="$(echo "$i" | perl -pe 's/(\d+)(?!.*\d)//g')"
      # echo -e "$nodeDirectory"
        if [ ! -e $workingDir/${striptBenchName}Data/$nodeDirectory/.success ] 
        then
            # This gets the qstat info for the specific JOBID by grepping the overall qstat for the ID
            eval "${benchName}Qstat=\$(echo -e \"\$qstatQuery\" | grep -e \"\$${benchName}JOBID\")"
            # Checks if the job is inqueue (set to 1 if in queue, set to 0 if finished)
            # grep -v means NOT to grep if contains pattern
            eval $(echo -e "${benchName}inqueue=\$(echo -e  \"\$${benchName}Qstat\" | grep -v \"finished\" | wc -l )")
            # temp integer to evaluate the value 
            eval "temp=${benchName}inqueue"
            # sets a finished variable to 1 if the grep contains finished
            if [ $temp -eq 0 ]
                then
                    eval "${benchName}finished=\$(echo -e \"\$${benchName}Qstat\" | grep \"finished\" | wc -l )"
            # Gets its status in the queue if it is inqueue
            elif [ $temp -eq 1 ]
                then
                    # Returns status as Q, R or E
                    eval $(echo -e "${benchName}Status=\$(echo -e \"\$${benchName}Qstat\" | tail -1  | awk '{print \$5}' )")
            fi
        fi
        generalStatusUpdate "$benchName"
    done
}

# This function prints status information for the current benchmark, including it's 
# queue status, whether the job has failed or whether the job has complete.
function generalStatusUpdate {
           # temp variable is needed to ensure integer expression is used and can be evaluated
           eval "tempInqueue=\$(echo \"\$${1}inqueue\")"
           eval "tempFlag=\$(echo \"\$${1}Flag\")"
           eval "tempFinished=\$(echo \"\$${1}finished\")"
           eval "tempJobID=\$(echo \"\$${1}JOBID\")"

           # gets the benchmark basename. Argument might be pt2pt2 or collective16. This will
           # strip the trailing node amounts and make sure the correct directory is checked for a success file
           export striptBenchName="$(echo "${1}" | perl -pe 's/(\d+)(?!.*\d)//g')"

           # if there is no success file and the job is still in queue, then it is still in progress
           if [ ! -e $workingDir/${striptBenchName}Data/$nodeDirectory/.success ] && [ $tempInqueue -ge 1 ]
            then 
                              
                printf "${1} benchmark in Progress...\n" 
                eval "tempStatus=\$(echo \"\$${1}Status\")"
                printf "${1} Status is: $tempStatus \n" 
            # if there is no success file but the finished flag is set, then the job has failed
            elif [ ! -e $workingDir/${striptBenchName}Data/$nodeDirectory/.success ] && [ $tempFinished -eq 1 ] 
            then
                printf "**${1} job $tempJobID has failed.**\n"
                eval "unset ${1}JOBID" 
                benchmarkNames
            # If the success file exists then the job has successfully completed 
            elif [ -e $workingDir/${striptBenchName}Data/$nodeDirectory/.success ] 
            then
            # Sets a flag to 1, so the job complete status does not iteratively print
            eval "tempFlag=${1}Flag"
                if [ $tempFlag -eq 0 ]
                    then 
                    printf "**${1} job $tempJobID is complete.**\n"
                    let "${1}Flag=1"
                fi
            fi
}

# Will initiate a countdown to start the test
function startCount {
    printf "\n----- Testing Suite -----\n"
        for i in {5..1}
        do
           printf "Starting Benchmark(s) in $i\n"
           sleep 1     
        done
    printf "\n"
}

# feedback time can be used to dictate the frequency of the qstat. 
# There will be 1 qstat per cycle. 
function feedbackTime {
    for i in {1..30}
        do
            echo -n "."
            sleep "$queueCycleTimeFloat"  
        done     
    echo -ne "\n"
}

# Will wait 3 seconds before performing a comparison to ensure all files 
# have been written 
function testComplete {
    printf "\n----- Tests Complete -----\n"

        for i in {3..1}
        do
           printf "Performing Comparison in $i\n"
           sleep 1     
        done

}

# This function contains the loop which controls the queue status
# If a job fails then the program will resume
function successChecks {

    eval "jobsComplete=0"

    # Gets benchmark names based off their submissions    
    benchmarkNames
    # sets each benchmark completion flag to 0
    setFlag "$jobIDs"

    while true
        do

        if [ "$jobsComplete" == "$numOfJobs" ]; then
         break 
        fi
        echo "Querying qstat..."
        generalQStat "$jobIDs"
        feedbackTime  
        jobsComplete=$(wc -l $workingDir/.successCheck | awk '{print $1}')  

        done  

}

# Gets each benchmarks name and counts the number of jobs submitted
function benchmarkNames {
    export jobIDs=$(printenv | grep ".*JOBID.*" | perl -ple 's/(.*)JOBID=.*/$1/g')
    export numOfJobs=$(echo $jobIDs | wc -w)
    if [ "$numOfJobs" == 0 ]; then 
        echo -e "Exiting Suite... "
        exit 1
    fi
}

# Defines each benchmarks completion flag as 0
# Takes the benchmark name as an input argument.
function setFlag {
    for benchName in $(echo "${1}") ;
        do
            eval "${benchName}Flag=0"
        done 
}

