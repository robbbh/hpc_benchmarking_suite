Graphing Your Data
==================

Using makeHistogram.py
----------------------

Once bulk data has been gathered, the user can now look at extracting information from it. 
Due to the nature of the task at hand, the best way of defining KGO ranges is by observing 
its distrobution in a histogram. Luckily the process of graphing this data is very straight 
forward and requires the user to simply provide a path to the bulk data.  

To graph the data, the user must be inside the **./makeKGO/** directory, where they will find a python 
file named **./makeHistogram.py/**. The user must then execute this script by giving it an argument to 
the data's location (including the files name).

The following example will graph all of the bulk data files for 128 node MVAPich collective jobs.

>>> ./makeHistogram.py ./mvaPichMakeKGO/collective/128/*.txt


The user should then see an output similar to the following. The histograms will be produced as **'.png'**
files and will be located in the same directory as the original data.

.. code-block:: text

    Sorting ./mvaPichMakeKGO/collective/128/osu_allreduce_run_32_4096.txt
    ./mvaPichMakeKGO/collective/128/osu_allreduce_run_32_4096.png has been saved to file
    Sorting ./mvaPichMakeKGO/collective/128/osu_alltoallv_run_32_4096.txt
    ./mvaPichMakeKGO/collective/128/osu_alltoallv_run_32_4096.png has been saved to file
    Sorting ./mvaPichMakeKGO/collective/128/osu_bcast_run_32_4096.txt
    ./mvaPichMakeKGO/collective/128/osu_bcast_run_32_4096.png has been saved to file
    Sorting ./mvaPichMakeKGO/collective/128/osu_gather_run_32_4096.txt
    ./mvaPichMakeKGO/collective/128/osu_gather_run_32_4096.png has been saved to file
    Sorting ./mvaPichMakeKGO/collective/128/osu_scatter_run_32_4096.txt
    ./mvaPichMakeKGO/collective/128/osu_scatter_run_32_4096.png has been saved to file

Here is an example of the type of graph you would expect to see. This repesent bulk data for an AlltoAllv benchmark 
running on 64 nodes.

.. image:: osu_alltoallv_run_32_2048.png


