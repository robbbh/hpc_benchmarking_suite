MVAPICH and EPCC KGO Submission Scripts
=======================================

    .. Warning::
        It is strongly advised that the user **NOT** substantially modify the script below, as it may break or cause unexpected errors when trying to run it. 

EPCC and MVAPICH KGO submission Source
--------------------------------------

Below is the **makeKGO** job submit script. This script is where MVAPICH and EPCC jobs
can be easily configured and ready to be mounted to a cron tab. 

Please see the comments in the code for a further description of what is going on and 
see the :doc:`developer guide </developer_guide>` for information on editing it.  
 
.. literalinclude:: ../../makeKGO/runScripts.sh
   :language: bash

Make MVAPICH Pt2pt KGO
----------------------

The below script is executed from the **runScripts** submission script.


.. literalinclude:: ../../makeKGO/mvaPichMakeKGO/getPt2ptData.sh
   :language: bash

Make MVAPICH Collective KGO
---------------------------

The below script is executed from the **runScripts** submission script.

.. literalinclude:: ../../makeKGO/mvaPichMakeKGO/getCollectiveData.sh
   :language: bash

Make EPCC Mixed Mode pt2pt KGO
------------------------------

The below script is executed from the **runScripts** submission script.

.. literalinclude:: ../../makeKGO/epccMakeKGO/getEpccPt2ptData.sh
   :language: bash

Make EPCC Mixed Mode Collective KGO
-----------------------------------

The below script is executed from the **runScripts** submission script.

.. literalinclude:: ../../makeKGO/epccMakeKGO/getEpccCollectiveData.sh
   :language: bash
