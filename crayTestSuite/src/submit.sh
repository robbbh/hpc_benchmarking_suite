#!/bin/bash
set -e

# this will bring the functions in submitFunc and submitBench into scope.
. submitFunc.sh
. submitBench.sh

# Derives the directory name from the time/date as it acts as 
# a unique identifier for all jobs run at that time.  
export mainDirecName=$(date -u +%Y-%m-%dT%H-%M-%SZ)
export currentDir=$(pwd)
export workingDir=$(echo $currentDir/jobsRun/$mainDirecName/)

export pt2ptFLAG=0
export collectiveFLAG=0
export streamMPIFLAG=0
export streamOMPFLAG=0
export epccCollectiveFlag=0
export epccPt2ptFlag=0
export options_found=0


# This will enable to use of flags while submitting benchmarks. 
# More flags will be added as more benchmarks are added
while getopts "AEcpSmohMle:" FLAG; do
  export options_found=1
  case $FLAG in
    A) 
        echo -e "Attempting to Submit All Currently Supported Benchmarks..."
        export pt2ptFLAG=1
        export collectiveFLAG=1
        export streamMPIFLAG=1
        export streamOMPFLAG=1
        export epccCollectiveFlag=1
        export epccPt2ptFlag=1  
        export allFlag=1
        
      ;;
    c)  
        echo -e "Attempting to Submit MVAPICH Collective Benchmarks..."
        export collectiveFLAG=1
      ;;
    p)  
        echo -e "Attempting to Submit MVAPICH pt2pt Benchmarks..."
        export pt2ptFLAG=1
      ;;
    M)  
        echo -e "Attempting to Submit All MVAPICH Benchmarks..."
        export pt2ptFLAG=1
        export collectiveFLAG=1
      ;;
    E)
        echo -e "Attempting to Submit All EPCC Benchmarks..."
        export epccPt2ptFlag=1
        export epccCollectiveFlag=1
        export allFlag=1 
      ;;
    m)  
      echo -e "Attempting to Submit Stream MPI Benchmark..."
        export streamMPIFLAG=1
      ;;
    o)  
      echo -e "Attempting to Submit Stream OpenMP Benchmark..."
        export streamOMPFLAG=1 
      ;;
    S)  
      echo -e "Attempting to Submit Stream Benchmarks..."
        export streamMPIFLAG=1
        export streamOMPFLAG=1       
      ;;
    e)
      echo -e "Attempting to Submit EPCC Collective Benchmarks..."
        
        if [ $OPTARG == "T" -o $OPTARG == "t" ]; then
            export epccCollectiveArg=$OPTARG
            export epccCollectiveFlag=1
        elif [ $OPTARG == "M" -o $OPTARG == "m" ]; then
            export epccCollectiveArg=$OPTARG
            export epccCollectiveFlag=1
        elif [ $OPTARG == "X" -o $OPTARG == "x" ]; then
            export epccCollectiveArg=$OPTARG
            export epccCollectiveFlag=1
        else
            echo -e "No Valid Configuration Given\n"
            echo -e "Valid Arguments:\nT / t = Threading Only\nM / m = MPI Only\nX / x = Mixed Mode\n"
            exit 1
        fi
      ;;
    l)
      echo -e "Attempting to Submit EPCC Pt2pt Benchmarks..."      
      export epccPt2ptFlag=1        
      ;;
    h)  
        HELP
      ;;
    \?) 
      echo -e "Unrecognised Flag. Please supply a valid flag\n"
      HELP
      echo "Exiting."
      exit 1

      ;;
  esac
done

if ((!options_found)); then
    echo "No Valid Flags Found. Please supply a valid flag."
    HELP
    echo "Exiting."
    exit 1
fi

# This makes the directories required for the run output.
# This allows me to differentiate between pt2pt and collective tests

mkdir $workingDir

# For each new benchmark a directory must be created for its data to be stored.
# IT is vitsal that the names of the benchmark data directory follow the naming 
# format of <benchName>Data or the comparison script will not be able to find 
# the jobs data. 

# Creates the empty success file which jobs will write to once they've completed.
# This file is vital as its contents regulate the while loop (keep it looping).

# Note: There are unique success files for each Job which are used to tell if a job 
#       has finished. 

#       The success file below is a "Global success" file as such and is used to tell
#       if all jobs are finished so the while loop may exit.

touch $workingDir/.successCheck

#jobs to run on the cray

#Current Benchmarks supported for broadwell: 
#               OSU collective 
#               OSU pt2pt
#               stream MPI & OMP

# CD's into the unique working directory for the job submission, so that $PBS_O_WORKDIR is set to correct path.
cd $workingDir

#:coretype=broadwell

# set this dictate the wait time between qstat query cycles. 
queueCycleTime=30

# ensures only 30 characters are printed when giving query feedback
export queueCycleTimeFloat=$(echo $((queueCycleTime / 30)))

# Wallclock times (might delegate it to another file)
export pt2ptTime2=00:05:00
export collectiveTime4=00:10:00
export collectiveTime8=00:15:00
export collectiveTime16=00:35:00
export collectiveTime32=00:45:00
export collectiveTime64=01:10:00
export collectiveTime128=01:40:00
export streamMPITime1=00:05:00
export streamOMPTime1=00:05:00
export epccCollectiveTime4=01:00:00
export epccCollectiveTime8=01:00:00
export epccCollectiveTime16=01:00:00
export epccPt2ptTime2=00:30:00

echo -e "------------------------------"

# Execution Flags

if ((pt2ptFLAG)); then
    pt2ptSubmit
fi

if ((collectiveFLAG)); then
    collectiveSubmit
fi

if ((streamMPIFLAG)); then
    streamMPISubmit
fi

if ((streamOMPFLAG)); then
    streamOMPSubmit
fi

if ((epccCollectiveFlag)); then
    # If the All flag is given, set the epcc collective arg to "X" (All benchmarks)
    if ((allFlag)); then
    epccCollectiveMode "X"
    else
    epccCollectiveMode $epccCollectiveArg    
    fi
fi

if ((epccPt2ptFlag)); then
    epccPt2ptSubmit
fi

echo -e "------------------------------"

# benchmark names will derive the benchmark names from the JOBID variables
# This will then pass these names to the successCheck function which will
# perform the status checking 
successChecks

# This function will ensure all files have been written by waiting 5 seconds 
# before performing compare.py
testComplete

# Runs the compare.py python script with the produced directory argument (The directory
# that is made when this script is executed). 
# It can take multiple arguments containing specific benchmarks following the directory, 
# however if no extra args are given it will 
# compare every benchmark with known good data results

$currentDir'/compare.py' $mainDirecName
