Make Known Good Output for EPCC and MVAPICH benchmarks
======================================================

The HPC Benchmarking Suite requires known good output (KGO) for each benchmark and its subsequent configurations.
This allows for an evaluation of the current performance of the machine against observed normal behaviour. 
Over time the HPC's configuration and hardware will change, resulting in the KGO's being outdated. This guide will 
explain to the user how they can produce and interpret their own MVAPICH and EPCC KGO data and how to implement it
correctly into the suite.

This process requires the user to install a crontab which can be scheduled by cron. A cron process will consistently submit jobs to 
the HPC on a user defined schedule; gradually building up a file of bulk data which can be graphed and interpreted. 
The current method of defining good data ranges is too observe the bulk data output plotted in histograms. The /makeKGO/ 
contains a python script which can produce these histograms.

Configuring the runScripts File
-------------------------------

Defining the Root directory
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The first step to making your own KGO is to explicitly define the root directory path in "/runScripts.sh" (*located inside the ./makeKGO/ directory*) script. 
This would involve connecting your $HOME directory (*denoted by a tilde "~"*) to the test suite itself (*./crayTestSuite*). The user would be required to 
input the intermediary path to these lcoations by defining the *rootPath* variable.

.. code-block:: bash

    # ~/<insert path here>
    export rootPath=~/trunk

By defining *~/trunk* as my intermidiary, my full path would look like */small/home/d05/rharris/trunk/crayTestSuite/makeKGO/mvaPichMakeKGO* 
when the script is run.

.. Warning:: The user must provide the root path to the suite's directory, or crontab will not know where the **/runScripts.sh** is located.
    This is a consequence of crontab being run by the daemon crond; which in effect does not source the standard environment.  

Deciding Which KGO to run
^^^^^^^^^^^^^^^^^^^^^^^^^

The second step to making your own KGO is to decide which benchmarks you wish to produce KGO for. By default KGO's for all benchmarks will
be collected if the user hasn't specified otherwise. 

In **/runScripts.sh** the user can choose not to collect data for a specific benchmark by modifying any the following flags to 0:

.. code-block:: bash

    mvapichCollective=1
    mvapichPt2pt=1
    epccCollective=1
    epccPt2pt=1

Running the crontab
-------------------

After configuring the *runScripts* file, the user must then install a crontab on one of the HPC login nodes. 
The recommended configuration is to install a crontab which will execute every 2 hours, however it is up to 
the user how frequent on infrequent they want to produce this data. 

The user must then input a **static** path to the *runScripts* file. The example below shows *runScripts* gathering 
KGO's at top of the hour, every other hour (12 times a day).

.. code-block:: bash

    00 00,02,04,06,08,10,12,14,16,18,20,22 * * *  /home/d05/rharris/trunk/crayTestSuite/makeKGO/runScripts.sh

You can find out more about cron and the crontab syntax by typing *more cron* into any terminal.
