#!/bin/bash
#PBS -N mvapichP2P
#PBS -q parallel


set -e

export MPICH_MAX_THREAD_SAFETY="multiple"
export OMP_NUM_THREADS=1
export MPICH_RANK_REORDER_DISPLAY=1
export execPath="../../benchmarks/osu-micro-benchmarks-5.1/mpi/pt2pt"
export metaName=$(echo $(date) | sed -e 's/\s/_/g' )
export bulkDataPath="../mvaPichMakeKGO/pt2pt/"
export uniqueJobPath="../mvaPichMakeKGO/pt2pt/$mainDirecName"

declare -i noMPIt 
declare -i TnoMPIt
# Number of MPI tasks per node
noMPIt=1 
# Total number of MPI tasks
# Gets number of nodes from the parent shell
TnoMPIt=$(echo "$(($pt2ptNodes * $noMPIt))")

cd $PBS_O_WORKDIR

mkdir $uniqueJobPath
export executable="osu_bibw osu_bw osu_latency osu_multi_lat osu_latency_mt" 

## Outputs which hall the Operational Suite is running in.
echo '' >> $uniqueJobPath/$metaName.temp 
cat /critical/opfc/hall_status_oper >> $uniqueJobPath/$metaName.temp 
echo '' >> $uniqueJobPath/$metaName.temp 


   for task in $executable
	do
        
        ## Appends the job name to the job dump file
        echo '' >> $uniqueJobPath/$metaName.temp 
        echo $task >> $uniqueJobPath/$metaName.temp 
        echo '' >> $uniqueJobPath/$metaName.temp 

            ## Runs the executable and outputs it to a file
            aprun -n $TnoMPIt -N $noMPIt $execPath/$task > $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/Application/d' $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/#/d' $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat

            cat $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat >> $bulkDataPath/${task}_run_${noMPIt}_${TnoMPIt}.txt
            echo '' >> $bulkDataPath/${task}_run_${noMPIt}_${TnoMPIt}.txt

        ## Raw Data set with JOB ID's for verification
        echo $PBS_JOBID >> $uniqueJobPath/$metaName.temp 
        cat $uniqueJobPath/${task}_run_${noMPIt}_${TnoMPIt}.dat >> $uniqueJobPath/$metaName.temp 
        echo '' >> $uniqueJobPath/$metaName.temp 

                        

        done

## Appends a snapshot of the machine state at the time the jobs ran
echo '' >> $uniqueJobPath/$metaName.temp 
qstat_snapshot >> $uniqueJobPath/$metaName.temp 
echo '' >> $uniqueJobPath/$metaName.temp 



