#!/bin/bash
#PBS -N EPCC_Collective
#PBS -q parallel

set -e

export execPath="../../benchmarks/epcc_mixedMode_Benchmarks/mixedModeBenchmark"
export metaName=$(echo $(date) | sed -e 's/\s/_/g' )
export bulkDataPath="./collective2/$epccCollectiveNodes"
export uniqueJobPath="./collective2/$epccCollectiveNodes/$mainDirecName"


#set variables as Intergers
declare -i MPItasks
declare -i task



export OMP_STACKSIZE=2G
export OMP_WAIT_POLICY=PASSIVE
export MPICH_MAX_THREAD_SAFETY=multiple
ulimit -s unlimited
export OMP_NUM_THREADS=1
export maxCores=36
export nodeCount=$epccCollectiveNodes
export totalMPItasks=$(($nodeCount * $MPItasks))
export MPITaskSet="36 18 12 6 4 2" 


cd $PBS_O_WORKDIR

mkdir $uniqueJobPath

ulimit -s unlimited

loopNo=0
for task in $MPITaskSet; 
     do



        let totalMPItasks=$(echo "$task*$nodeCount" | bc)
        let loopNo=$loopNo+1
        let splitNuma=$task/2

        export OMP_NUM_THREADS=$(($maxCores / $task))

        # MPI = 36 18 12 6 4 2
        # OMP = 1 2 3 6 9 18
        echo -e "Total num of MPI tasks: $totalMPItasks" 
        echo -e "MPI Tasks: $task"
        echo -e "Threads: $OMP_NUM_THREADS"
        echo -e "Iteration Num: $loopNo"
        echo -e "numa split $splitNuma\n"

        mkdir $uniqueJobPath/$OMP_NUM_THREADS
        aprun -n $totalMPItasks -N $task -cc numa_node -ss -d $OMP_NUM_THREADS -S $splitNuma \
        $execPath configure_collective.txt \
         > $uniqueJobPath/$OMP_NUM_THREADS/epcc_Collective_${loopNo}.txt


         sed -i -e '/Application/d' $uniqueJobPath/$OMP_NUM_THREADS/epcc_Collective_${loopNo}.txt

        # PATH: /home/d05/rharris/trunk/crayTestSuite/makeKGO/epccMakeKGO/collective/4/name
         ./mixedModeParser.py $uniqueJobPath/$OMP_NUM_THREADS/epcc_Collective_${loopNo}.txt

    done

