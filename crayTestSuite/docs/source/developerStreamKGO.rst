Make Known Good Output for STREAM Benchmarks
============================================

This guide will show the user how to produce their own KGO for the STREAM benchmark. Unlike
the MVAPICH and EPCC benchmarks, making STREAM KGO will only require one run job submission. 
This is because it's measuring indiviual node performance for a specific node type, where the
results should be very similar.

The suite will gather STREAM KGO by submitting the STREAM benchmark 'n' amount of times, with
'n' being the user defined amount of nodes. By default the node count is 32 as it gives a 
reasonable sample data. The results will the be outputted to **csv** files, which will the be
processed by a KGO range producing python script.

Define Node Counts
------------------

Too change the default node counts, the user must edit the *streamMPINodes* and *streamOMPNodes* variables 
in the **produceStreamKGO.sh** script. The variables can be seen in bold below. 

.. parsed-literal::

    export **streamMPINodes=32**
    export streamMPI1JOBID=$(qsub -V -l select="$streamMPINodes":coretype=broadwell -l walltime=00:05:00 -j oe -o ./mpi/ ./getStreamMPI.sh)
    echo "STREAM MPI get KGO job submitted with jobID $streamMPI1JOBID"

    export **streamOMPNodes=32**
    export streamOMP1JOBID=$(qsub -V -l select="$streamOMPNodes":coretype=broadwell -l walltime=00:05:00 -j oe -o ./omp/ ./getStreamOMP.sh)
    echo "STREAM OMP get KGO job submitted with jobID $streamOMP1JOBID"

Submit the Benchmarks
---------------------

To submit the benchmarks, the user must execute the **produceStreamKGO.sh** script located in
the **makeKGO/stream** directory. Once the script has been executed, the user should see the following
output

.. parsed-literal::

    STREAM MPI get KGO job submitted with jobID 2800935.xce00
    STREAM OMP get KGO job submitted with jobID 2800936.xce00

The benchmarks should take around 60 seconds to run and to be processed.

Locating the KGO Ranges
-----------------------

The KGO ranges will the be available in either **/mpi** or **/omp**
with the file name **STREAM_mpi_KGO** or **STREAM_omp_KGO**. The file
output will look similar to the following.

.. parsed-literal::

    KGO for the the following file ./mpi/Add_dump.csv
    98050.9962371 99347.1162629

    KGO for the the following file ./mpi/Copy_dump.csv
    85725.5646432 86987.3291068

    KGO for the the following file ./mpi/Scale_dump.csv
    85924.6774634 86861.1225366

    KGO for the the following file ./mpi/Triad_dump.csv
    97994.8026681 99440.3473319



These ranges will now be ready to insert into the suites 
**src/kgo** directory. For information on inserting the kgo's 
into files, please see :ref:`setting-kgo`.

