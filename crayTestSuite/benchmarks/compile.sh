#!/bin/bash
# Builds all currently supported benchmarks

root=$PWD

echo -e "Entering '/osu-micro-benchmarks-5.1/'"
cd ./osu-micro-benchmarks-5.1/
echo -e "Making build.sh Executable. "
chmod +x ./build.sh
echo -e "Executing build.sh.\n"
./build.sh


cd $root

# Stream MPI version
# Do not change this value as it is the only one currently supported
# Array size is equal to 4GB

export ArraySizeMPI=4294967296
echo -e "\nEntering '/stream/streamMPI/'"
cd ./stream/streamMPI/

echo -e "Attempting to compile stream_mpi with an Array Size of $ArraySizeMPI"
cc -O3 -homp -DSTREAM_ARRAY_SIZE="$ArraySizeMPI" stream_mpi.c -o streamMPI
if [ "$?" -eq 0 ] ; then
    echo -e "\nCompilation of StreamMPI Succesful with Array Size $ArraySizeMPI."
else
    echo -e "\nCompilation error while building StreamMPI with Array Size $ArraySizeMPI."
    exit 1
fi

cd $root

# stream OMP version
# Do not change this value as it is the only one currently supported 
# Array size is equal to 4GB

export ArraySizeOMP=4294967296
echo -e "\nEntering '/stream/streamOMP/'"
cd ./stream/streamOMP/

echo -e "Attempting to compile stream_omp with an Array Size of $ArraySizeOMP"
cc -O3 -homp -hpic -hdynamic -DSTREAM_ARRAY_SIZE="$ArraySizeOMP" ./stream_omp.c -o ./streamOMP
if [ "$?" -eq 0 ] ; then
    echo -e "\nCompilation of StreamOMP Succesful with Array Size $ArraySizeOMP."
else
    echo -e "\nCompilation error while building StreamOMP with Array Size $ArraySizeOMP."
    exit 1
fi

echo -e "\nEntering '/epcc_mixedMode_Benchmarks/'"
cd $root/epcc_mixedMode_Benchmarks

make
if [ "$?" -eq 0 ] ; then
    echo -e "\nCompilation EPCC Mixed Mode Benchmarks Successful."
else
    echo -e "\nCompilation error while building EPCC Mixed Mode Benchmarks Successful."
    exit 1
fi

cd $root

echo -e "\n ----- Compilation Complete! ----- \n"

# More benchmarks to be added in later versions.
