User Guide Basics
=================

Welcome to the basic tutorial guide for the HPC benchmarking Suite.

.. Warning:: This tutorial assumes you have the suite installed on a 
    directory that is mounted on the HPC. If you are running the suite
    locally it will not work. It also assumes that the user is working 
    on a HPC with a PBS scheduler. If the scheduler isn't PBS then the 
    scripts will not be submitted.

Getting Started
---------------

.. Note:: The HPC Benchmkaring Suite currently has KGO's (Known Good Output) 
    for 4, 8, 16, 32, 64 and 128 node MVAPich collective , however the 
    default setup will only run 4 and 8 node runs. If you would like to increase 
    the collectice node count then please see the :doc:`developer guide </developer_guide>`. 

To start the user must first compile the benchmarks located in the "benchmarks" directory.
To do this the user must execute the "./compile.sh" script located in the benchmarks directory. 

.. Warning:: If the benchmarks haven't been compiled succesfully before use then aprun 
    will be unable to find the executable and kill the suite.   

Submitting a Job/Benchmark to the HPC
-------------------------------------

Benchmark submission to the HPC is done via the ./submit.sh BASH script which is located in ./src/ directory.
The script will only except a valid flag before it can be executed.

The currently supported flags are as followed:

.. parsed-literal::

    -A, All Benchmarks
    -S, All STREAM Benchmarks
    -M, All MVAPICH Benchmarks
    -E, All EPCC Mixed Mode Benchmarks
    -o, STREAM OpenMP Benchmark
    -m, STREAM MPI Benchmark
    -c, MVAPICH Collective Benchmarks
    -p, MVAPICH Pt2pt Benchmkarks
    -l, EPCC Mixed Mode Pt2pt
    -e, EPCC Mixed Mode Collective
	     "T" or "t" = Threading only
	     "M" or "m" = MPI only
	     "X" or "x" = Mixed Mode
    -h, HELP

.. Note:: The EPCC Mixed mode collective benchmark has 3 different submission 
    types which include threading only, MPI only and mixed mode runs. It is 
    also worth noting that the "A" flag will submit the epcc collective benchmarks 
    with the mixed mode flag.

The submission example below shows the EPCC collective benchmark being submitted
with the Mixed Mode option enabled.

>>> ./submit.sh -e x

The following submission example will submit all MVAPICH benchmarks. This 
includes both collective and pt2pt benchmarks.

>>> ./submit.sh -M

Expected output :

.. parsed-literal:: 

    Attempting to Submit All MVAPICH Benchmarks...
    ------------------------------
    MVAPICH pt2pt Submitted
    MVAPICH Collective Submitted
    ------------------------------
    Querying qstat...
    pt2pt2 benchmark in Progress...
    pt2pt2 Status is: **Q** 
    collective4 benchmark in Progress...
    collective4 Status is: **Q** 
    ..............................
    Querying qstat...
    pt2pt2 benchmark in Progress...
    pt2pt2 Status is: **R** 
    collective4 benchmark in Progress...
    collective4 Status is: **R** 
    ..............................


If a benchmark has finished then its status in the queue will appear as follows;

.. parsed-literal:: 

    Querying qstat...
    collective4 benchmark in Progress...
    collective4 Status is: **R** 
    pt2pt2 benchmark in Progress...
    pt2pt2 Status is: **R** 
    ..............................
    Querying qstat...
    collective4 benchmark in Progress...
    collective4 Status is: **R** 
    **pt2pt2 job 8728334.xce00 is complete.**

If a benchmark has failed then its status in the queue will appear as follows;

.. parsed-literal:: 

    Querying qstat...
    collective4 benchmark in Progress...
    collective4 Status is: **R** 
    pt2pt2 benchmark in Progress...
    pt2pt2 Status is: **R** 
    ..............................
    Querying qstat...
    collective4 benchmark in Progress...
    collective4 Status is: **R** 
    **pt2pt2 job 8728334.xce00 has failed.**

Once all benchmarks have finished the comparison script will be automatically run with the 
following output printed to screen. This output will also be written to a summary, output 
and error file within the job directory.

.. parsed-literal:: 

    ...........

     --------------------------------------------------------------------------------
    |     Benchmark Name      |              osu_multi_lat_run_1_2.dat               |
     --------------------------------------------------------------------------------
    |         Result          |                Passed with 1 Error(s)                |
     --------------------------------------------------------------------------------
    |     Nodes     |   Total MPI Tasks    |     MPI Tasks      |      Threads       |
     ---------------+----------------------+--------------------+-------------------- 
    |       2       |          2           |         1          |         1          |
     --------------------------------------------------------------------------------


     -------------------------------------------------------
    |                     Quick Summary                     |
     -------------------------------------------------------
    |          Pass          |              7               |
     -------------------------------------------------------
    |     Pass \\w Errors     |              2               |
     -------------------------------------------------------
    |          Fail          |              1               |
     -------------------------------------------------------
    |  Total Benchmarks Run  |              10              |
     -------------------------------------------------------

    ---------- A summary report has been produced in the job directory ----------

    ---------- A data output report has been produced in the job directory ----------

    ---------- A data error report has been produced in the job directory ----------

.. Note:: It is not cumpolsory to track the benchmarks' running status, and the process
    can be stopped at any time. The jobs will still complete, however you will have to 
    run the comparison process manually. A guide on how to do this can 
    be found at `Using the Comparison Script Manually`_.

Using the Comparison Script Manually
------------------------------------

.. Warning:: The comparison script assumes you have already submitted 
    and completed a successful job run. If there are incomplete jobs
    in the job directory then the execution of the comparison script
    could cause errors.  

The script must be given a directory argument of which contains 
the directories and data of the benchmarks run. The directories 
name will be represented in a Date/ToD format (**2015-11-26T16-40-18Z**)
and must **always** be used as the first argument. If no valid directory
are given then an error will occur.

.. Note:: If a job has been submitted successfully or unsuccessfully,
    its job directory can be found in the **src/jobsRun/** directory.  

An example of using the comparison script:

>>> ./compare.py <Directory_Name>
>>> ./compare.py 2015-11-26T16-40-18Z

The above example will compare all benchmarks within the 
directory that have results. By supplying extra arguments
however, it is possible to specify individual benchmarks 
that one may want to compare.

for example:

>>> ./compare.py <Directory_Name> benchName1 benchName2 ...
>>> ./compare.py 2015-11-26T16-40-18Z pt2pt collective

The above example will only compare the pt2pt and collective benchmarks
and disregard benchmarks not specified.

.. Note:: 
    The benchmark name is represented by the directory name which the 
    live data is outputted too. For example, the point-to-point MPICH 
    benchmark's output directory name is "pt2pt" meaning "pt2pt" 
    will be valid as an argument. 



