#!/bin/bash
#PBS -N mvapichCollect
#PBS -q parallel

set -e

# workingDir $ curentDir are exported from the parent shell
export OMP_NUM_THREADS=1
export mainJobDir="$workingDir/collectiveData"
export subJobDir="$mainJobDir/$collectiveNodes"
export filePath="collectiveData/$collectiveNodes/$OMP_NUM_THREADS"

# If the main directory hasn't been created, create it
if [ ! -d "$mainJobDir" ]; then
    mkdir "$mainJobDir"
fi 
mkdir "$subJobDir"
mkdir "$subJobDir/$OMP_NUM_THREADS"


# export MPICH_RANK_REORDER_DISPLAY=1
export execPath="$currentDir/../benchmarks/osu-micro-benchmarks-5.1/mpi/collective"

declare -i noMPIt 
declare -i TnoMPIt
# Number of MPI tasks per node
noMPIt=32 
# Total number of MPI tasks
# Gets number of nodes from the parent shell
TnoMPIt=$(echo "$(($collectiveNodes * $noMPIt))")

cd $PBS_O_WORKDIR

export executable="osu_allreduce osu_bcast osu_gather osu_scatter osu_alltoallv" 

	for task in $executable 
	do
            aprun -n $TnoMPIt -N $noMPIt $execPath/$task 1> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/^$/d' $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/#/d' $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            sed -i -e '/Application/d' $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            # Add metadata to the live run file
            echo "# Threads = $OMP_NUM_THREADS" >> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            echo "# MPI Tasks = $noMPIt" >> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            echo "# Total MPI Tasks = $TnoMPIt" >> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
            echo "# Nodes = $collectiveNodes" >> $PBS_O_WORKDIR/$filePath/${task}_run_${noMPIt}_${TnoMPIt}.dat
        done 
    # Writes to the local completion file 
    touch "$PBS_O_WORKDIR/collectiveData/$collectiveNodes/.success"
    # Writes to the global completion file
    echo "collective" >> $PBS_O_WORKDIR'/.successCheck'

