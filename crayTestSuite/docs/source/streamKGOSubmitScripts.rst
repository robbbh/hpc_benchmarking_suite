STREAM KGO Script Source
========================

Below is the STREAM job submit script which will call both STREAM MPI and
STREAM OMP scripts. This script has been seperated from the **runScripts**
submission, as it only needs to be executed once meaning it does not need 
to be installed to a cron tab.

STREAM Submission Script
------------------------

The below script will call both STREAM submission scripts.

.. literalinclude:: ../../makeKGO/stream/produceStreamKGO.sh
   :language: bash

Get STREAM OMP KGO
------------------

.. literalinclude:: ../../makeKGO/stream/getStreamOMP.sh
   :language: bash

Get STREAM MPI KGO
------------------

.. literalinclude:: ../../makeKGO/stream/getStreamMPI.sh
   :language: bash
