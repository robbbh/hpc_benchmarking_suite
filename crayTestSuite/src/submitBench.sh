#!/bin/bash

# Submission Commands have been delegated here and will be called when the appropriate flags have been specified 


set -e

function pt2ptSubmit {
    export pt2ptNodes=2
    export pt2pt2JOBID=$(qsub -V -l select="$pt2ptNodes":coretype=broadwell -l walltime="$pt2ptTime2" -j oe -o $workingDir $currentDir/pt2ptRun.sh 2>&1)
    echo -e "MVAPICH pt2pt Submitted"
}

function collectiveSubmit {
    export nodes='4 8' # 16 32 64 128'
    for input in $nodes;
        do

            export collectiveNodes=$input
            # concatonates the walltime variable based off the node count
            eval "timeVar=\$collectiveTime$input"
            # evals, builds and submits the Job and stores as a jobID variable which can be tracked in the queue
            eval "export collective${input}JOBID=\$(qsub -V -l select=\"\$collectiveNodes\":coretype=broadwell -l walltime=\"\$timeVar\" -j oe -o \"\$workingDir\" $currentDir/collectiveRun.sh 2>&1)"

       done
    echo -e "MVAPICH Collective Submitted"
}

function streamMPISubmit {
    export streamMPINodes=1
    export streamMPI1JOBID=$(qsub -V -l select="$streamMPINodes":coretype=broadwell -l walltime="$streamMPITime1" -j oe -o $workingDir $currentDir/streamMPIRun.sh 2>&1)
    echo -e "STREAM MPI Submitted"
}

function streamOMPSubmit {
    export streamOMPNodes=1
    export streamOMP1JOBID=$(qsub -V -l select="$streamOMPNodes":coretype=broadwell -l walltime="$streamOMPTime1" -j oe -o $workingDir $currentDir/streamOMPRun.sh 2>&1)
    echo -e "STREAM OMP Submitted"
}

function epccPt2ptSubmit {

    export epccPt2ptNodes=2
    export epccOMPTasks='1 2 4 8 18'
    export epccPt2pt2JOBID=$(qsub -V -l select="$epccPt2ptNodes":coretype=broadwell -l walltime=$epccPt2ptTime2 -j oe -o $workingDir $currentDir/epccPt2ptRun.sh 2>&1)
    echo -e "EPCC Pt2pt Submitted"

}

function epccCollectiveSubmit {
    export epccMPITasks=${2}
    export epccNodes='4 8 16'
    for input in $epccNodes;
        do

            export epccCollectiveNodes=$input
            # concatonates the walltime variable based off the node count
            eval "timeVar=\$epccCollectiveTime$input"
            eval "export epccCollective${input}JOBID=\$(qsub -V -l select=\"\$epccCollectiveNodes\":coretype=broadwell -l walltime=\"\$timeVar\" -j oe -o \"\$workingDir\" $currentDir/epccCollectiveRun.sh 2>&1)"

        done     

    echo -e "EPCC Collective ${1} Submitted"

}

function epccCollectiveMode {
    argIn=${1}
    if [ $argIn == "T" -o $argIn == "t" ]; then
        epccCollectiveSubmit "Threading Only" "2"
    elif [ $argIn == "M" -o $argIn == "m" ]; then
        epccCollectiveSubmit "MPI Only" "36"
    elif [ $argIn == "X" -o $argIn == "x" ]; then
        epccCollectiveSubmit "Mixed Mode" "36 18 12 6 4 2"
    else
        echo -e "No valid Flag Given"
        exit 1
    fi
}

function HELP {
echo -e "----- Valid Flags -----\n"
echo -e "-A, All Benchmarks"
echo -e "-S, All STREAM Benchmarks"
echo -e "-M, All MVAPICH Benchmarks"
echo -e "-E, All EPCC Mixed Mode Benchmarks"
echo -e "-o, STREAM OpenMP Benchmark"
echo -e "-m, STREAM MPI Benchmark"
echo -e "-c, MVAPICH Collective Benchmarks"
echo -e "-p, MVAPICH Pt2pt Benchmkarks"
echo -e "-l, EPCC Mixed Mode Pt2pt"
echo -e "-e, EPCC Mixed Mode Collective"
echo -e "\t \"T\" or \"t\" = Threading only"
echo -e "\t \"M\" or \"m\" = MPI only"
echo -e "\t \"X\" or \"x\" = Mixed Mode"
echo -e "-h, HELP"
echo -e "\nFor documentation please see the /docs/ directory in this working copy"
exit 1
}
