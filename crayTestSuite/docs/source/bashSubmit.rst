Submit Script Source
====================

Job Submit Source
-----------------

Below is the job submit script. This script is where the jobs are submitted and configs are edited. 
Please see the script comments for a more indepth description of the code, and see the :doc:`developer guide </developer_guide>` for information on editing it.  
 
.. literalinclude:: ../../src/submit.sh
   :language: bash

Job Submit Function Source
--------------------------

Below is the submit scripts' functions. These form the main logic behind the user output and queue status querying. 

.. literalinclude:: ../../src/submitFunc.sh
   :language: bash

Job Submit Benchmarks Conifguration Source
------------------------------------------

Below is the submit scripts' functions. These form the submission parameters for each benchmark.

.. literalinclude:: ../../src/submitBench.sh
   :language: bash

