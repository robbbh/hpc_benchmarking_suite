#!/bin/bash
#PBS -N EPCC_Collective
#PBS -q parallel

set -e

# workingDir $ curentDir are exported from the parent shell
export OMP_NUM_THREADS=1
export mainJobDir="$workingDir/epccCollectiveData"
export subJobDir="$mainJobDir/$epccCollectiveNodes"


# If the main directory hasn't been created, create it
if [ ! -d "$mainJobDir" ]; then
    mkdir "$mainJobDir"
fi 
mkdir "$subJobDir"

export execPath="../../../benchmarks/epcc_mixedMode_Benchmarks/mixedModeBenchmark"
export execInputPath="../../../src/epccConfig/configure_collective.txt"


declare -i task

export OMP_STACKSIZE=2G
export OMP_WAIT_POLICY=PASSIVE
export MPICH_MAX_THREAD_SAFETY=multiple
ulimit -s unlimited
# broadwell nodes have 36 cores max
export maxCores=36
export nodeCount=$epccCollectiveNodes
export MPITaskSet=$epccMPITasks


cd $PBS_O_WORKDIR

ulimit -s unlimited

loopNo=0
for task in $MPITaskSet; 
     do

        let totalMPItasks=$(echo "$task*$nodeCount" | bc)
        let loopNo=$loopNo+1
        let splitNuma=$task/2

        export OMP_NUM_THREADS=$(($maxCores / $task))
        export filePath="epccCollectiveData/$epccCollectiveNodes/$OMP_NUM_THREADS"

        # MPI = 36 18 12 6 4 2
        # OMP = 1 2 3 6 9 18

        mkdir "$subJobDir/$OMP_NUM_THREADS"

        aprun -n $totalMPItasks -N $task -cc numa_node -ss -d $OMP_NUM_THREADS -S $splitNuma \
        $execPath $execInputPath \
        > $PBS_O_WORKDIR/$filePath/epcc_Collective_${loopNo}.txt



         sed -i -e '/Application/d' $PBS_O_WORKDIR/$filePath/epcc_Collective_${loopNo}.txt

        # Must pass the job output along with the number of nodes and MPI tasks
         ../../epccConfig/mixedModeParser.py $PBS_O_WORKDIR/$filePath/epcc_Collective_${loopNo}.txt \
         $epccCollectiveNodes $task

    done

    # Writes to the local completion file 
    touch "$PBS_O_WORKDIR/epccCollectiveData/$epccCollectiveNodes/.success"
    # Writes to the global completion file
    echo "epccCollective" >> $PBS_O_WORKDIR'/.successCheck'

